//////////////////////////////////////////////////
//             MC TPC: STORE DATA               //
// Can simulate and store more than 1 event.    //
// Needs a plotter based on mctpc.              //
//////////////////////////////////////////////////

#undef TAPPLICATION

// Standard C++ include files
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <algorithm>
#include <chrono> 
#include <functional>
#include <iostream>
#include <cmath>

// Root include files.
#include <TROOT.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TPaveLabel.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TRandom3.h>
#include <TFile.h>
#include <TTree.h>
#include <TObjArray.h>

// Garfield++, Heed, Magboltz
#include "Random.hh"
#include "MediumMagboltz.hh"
#include "FundamentalConstants.hh"
#include "SolidBox.hh"
#include "GeometrySimple.hh"
#include "Sensor.hh"
#include "TrackHeed.hh"

#include "Plotting.hh"
#include "ViewMedium.hh"
#include "ViewGeometry.hh"
#include "ViewField.hh"
#include "ViewCell.hh"
#include "ViewDrift.hh"
#include "Plotting.hh"

#include "ComponentAnalyticField.hh"
#include "ComponentConstant.hh"
#include "ComponentVoxel.hh"
#include "ComponentUser.hh"
#include "Component.hh"

#include "DriftLineRKF.hh"
#include "AvalancheMC.hh"
#include "AvalancheMicroscopic.hh"

/*******************************************************************************
 * GARFIELD++ and PHYSICS 
 * *******************************************************************************/
char gasf[256] = "new.gas";

/*******************************************************************************
 * MAIN
 * *******************************************************************************/
int main(int argc,char **argv) {

#ifdef TAPPLICATION
   TApplication app("app", &argc, argv);
#endif

   Garfield::MediumMagboltz* gas = new Garfield::MediumMagboltz();
   gas->SetComposition("Ar", 95., "CF4", 3., "iC4H10", 2.);
   gas->SetTemperature(293.15);
   gas->SetPressure(760.00);
   // Set the field range to be covered by the gas table.
   const int neFields = 6;
   const double emin =     32.;
   const double emax =    320.;
   constexpr bool useLog = true;
   // Flag to request logarithmic spacing.
   const int nbFields = 6;
   const double bmin =     0.0;
   const double bmax =     1.0;
   // Flag to request logarithmic spacing.
   const int nAngles  = 6;
   const double amin =     0.0;
   const double amax =     1.5707;
   gas->SetFieldGrid(emin, emax, neFields, useLog, bmin, bmax, nbFields, amin, amax, nAngles);
   const int ncoll = 10;
   // Run Magboltz to generate the gas table.
   gas->GenerateGasTable(ncoll);
   // Save the table.
   gas->WriteGasFile(gasf);

   Garfield::ViewMedium mediumView;
   mediumView.SetMedium(gas);
   mediumView.PlotElectronVelocity('e');

#ifdef TAPPLICATION
   //c1->SaveAs("plot.pdf");
   app.Run(true);
#endif

   return 0;
}


