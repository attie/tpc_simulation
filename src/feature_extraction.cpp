#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "TRandom2.h"
#include "TError.h"
#include <iostream>
#include <fstream>
#include "TH1F.h"
#include "TH2D.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h" 
#include "TF1Convolution.h"
#include "TRandom3.h"
#include "TLegend.h"


void feature_extraction(std::string filename1, std::string filename2) {
    TFile *f = TFile::Open((TString) filename1);
    TTree *t3 = (TTree*)f->Get("t3");
    ofstream o1(filename2);
    Int_t     nEvtId;
    const Int_t nMaxClu = 10000;
    Int_t     nClu;
    Double_t  xClu[nMaxClu]; Double_t  yClu[nMaxClu]; Double_t  zClu[nMaxClu]; Double_t  tClu[nMaxClu]; 
    Double_t  eClu[nMaxClu]; 
    Int_t    neClu[nMaxClu];
    
    const Int_t nMaxEle = 10000;
    Int_t     nEle;
    Double_t  xEle[nMaxEle]; Double_t  yEle[nMaxEle]; Double_t  zEle[nMaxEle]; Double_t  tEle[nMaxEle];
    Double_t xpEle[nMaxEle]; Double_t ypEle[nMaxEle]; Double_t zpEle[nMaxEle]; Double_t tpEle[nMaxEle];
    Double_t  gEle[nMaxEle];

    // sub pads activated
    const Int_t nMaxSub = 10000;
    Int_t     nSub;
    Double_t  xSub[nMaxSub]; Double_t  ySub[nMaxSub]; Double_t  qSub[nMaxSub]; Double_t  tSub[nMaxSub];

    // pad activated  
    const Int_t nMaxPad = 10000; 
    Int_t     nPad;
    double_t  xPad[nMaxPad]; Double_t  yPad[nMaxPad]; Double_t  qPad[nMaxPad]; Double_t  tPad[nMaxPad];

    t3->SetBranchAddress("id",&nEvtId);
    t3->SetBranchAddress("nc",&nClu);
    t3->SetBranchAddress("xc",xClu);
    t3->SetBranchAddress("yc",yClu);
    t3->SetBranchAddress("zc",zClu);
    t3->SetBranchAddress("tc",tClu);
    t3->SetBranchAddress("ec",eClu);
    t3->SetBranchAddress("nec",neClu);
    t3->SetBranchAddress("ne",&nEle);
    t3->SetBranchAddress("xe",xEle);
    t3->SetBranchAddress("ye",yEle);
    t3->SetBranchAddress("ze",zEle);
    t3->SetBranchAddress("te",tEle);
    t3->SetBranchAddress("xp",xpEle);
    t3->SetBranchAddress("yp",ypEle);
    t3->SetBranchAddress("zp",zpEle);
    t3->SetBranchAddress("tp",tpEle);
    t3->SetBranchAddress("ge",gEle);
    t3->SetBranchAddress("ns",&nSub);
    t3->SetBranchAddress("xs",xSub);
    t3->SetBranchAddress("ys",ySub);
    t3->SetBranchAddress("qs",qSub);
    t3->SetBranchAddress("ts",tSub);
    t3->SetBranchAddress("npd",&nPad);
    t3->SetBranchAddress("xpd",xPad);
    t3->SetBranchAddress("ypd",yPad);
    t3->SetBranchAddress("qpd",qPad);
    t3->SetBranchAddress("tpd",tPad);

    std::vector<int>* padn = 0;
    TBranch *bpadn = 0;
    t3->SetBranchAddress("padn",&padn,&bpadn);

    std::vector<int>* padp = 0;
    TBranch *bpadp = 0;
    t3->SetBranchAddress("padp",&padp,&bpadp);

    std::vector<vector<double>>* padwf = 0; 
    TBranch *bpadwf = 0;
    t3->SetBranchAddress("padwf",&padwf,&bpadwf);
      
    std::vector<vector<double>>* padwfi = 0; 
    TBranch *bpadwfi = 0;
    t3->SetBranchAddress("padwfi",&padwfi,&bpadwfi);
      
    std::vector<vector<double>>* padwfdf = 0; 
    TBranch *bpadwfdf = 0;
    t3->SetBranchAddress("padwfdf",&padwfdf,&bpadwfdf);
      
    std::vector<vector<double>>* padwfcv = 0; 
    TBranch *bpadwfcv = 0;
    t3->SetBranchAddress("padwfcv",&padwfcv,&bpadwfcv);



    for (Int_t i=0; i<t3->GetEntries(); i++) {
        t3->GetEntry(i);
        float max_wf[nMaxPad]={0.};
        float max_time[nMaxPad]={0.};

        int nclock=padwfcv->at(0).size();//MC clock ratio * sampling time
        o1<<nPad<<endl;
        /*for(int j=0;j<nPad;j++) {
            double maxwf=0;
            for(int k=0;k<nclock;k++) {
                if(padwfcv->at(j).at(k)>maxwf) {
                    maxwf=padwfcv->at(j).at(k);
                    max_wf[j]=maxwf;
                    max_time[j]=k;
                }
            }
        }
        */
        o1<<i<<" "<<nEle<<endl;
        
        for(int j=0;j<nPad;j++) {
        o1<<xPad[j]<<" "<<yPad[j]<<" "<<qPad[j]<<" "<<tPad[j]<<std::endl;
        }
    }
}


double x_pos[]={0.,1.,2.,3.,4.,5.,6.,7.,8.,9.,10.,11.,12.,13.,14.,15.,16.,17.,18.,19.,20.,21.,22.,23.,24.,25.,26.,27.,28.,29.,30.,31.,32.,33.,34.,35.,36.};
double y_pos[]={0.,1.,2.,3.,4.,5.,6.,7.,8.,9.,10.,11.,12.,13.,14.,15.,16.,17.,18.,19.,20.,21.,22.,23.,24.,25.,26.,27.,28.,29.,30.,31.,32.};

void create_file(std::string filename1, std::string filename2) {
    ifstream i1(filename1);
    ifstream i2(filename2);
    Int_t id=0;
    Double_t ampl[36][32]={0.};
    Double_t x[36][32]={0.};
    Double_t y[36][32]={0.};
    Double_t t_max[36][32]={0.};
    Double_t fwhm[36][32]={0.};
    Double_t dx=0, dy=0, dz=0;
    double a=0, b=0, c=0;
    int nPad=0, n_elec=0;

    TFile *f=new TFile("data_out.root","recreate");
    TTree *t= new TTree("t","");
    t->Branch("id",&id,"Int_t/I");
    t->Branch("ampl",&ampl,"Double_t[36][32]/D");
    t->Branch("t_max",&t_max,"Double_t[36][32]/D");
    t->Branch("fwhm",&fwhm,"Double_t[36][32]/D");
    t->Branch("x", &x, "Double_t[36][32]/D");
    t->Branch("y", &y, "Double_t[36][32]/D");
    t->Branch("dx",&dx,"Double_t/D");
    t->Branch("dy",&dy,"Double_t/D");
    t->Branch("dz",&dz,"Double_t/D");

    for(int i=0; i < 40; i++) {
        i1>>nPad;
        i1>>id;
        i1>>n_elec;
        if(n_elec==0)
            continue;
            
        for(Int_t j=0;j<nPad;j++) {
            i1>>id;
            i1>>a;
            i1>>b;
            i1>>c;
            ampl[(int)a][(int)b]=c;
            i1>>c;
            t_max[(int)a][(int)b]=c;
            x[(int)a][(int)b]=(x_pos[(int)a]+x_pos[(int)a+1])/2.;
            y[(int)a][(int)b]=(y_pos[(int)b]+y_pos[(int)b+1])/2.;

        }
        i2>>a;
        i2>>a;
        i2>>a;
        i2>>a;
        i2>>dx;
        i2>>dy;
        i2>>dz;
        t->Fill();
        memset(ampl, 0, sizeof(ampl));
        memset(t_max, 0, sizeof(t_max));
        memset(fwhm, 0, sizeof(fwhm));
        memset(x, 0, sizeof(x));
        memset(y, 0, sizeof(y));
        
    }    


    t->Write();
    f->Close();

}
