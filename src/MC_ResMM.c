#include "../inc/MC_ResMM.h"

#include <random>
std::default_random_engine generator;
std::uniform_real_distribution<double> distribution(0.0,1.0);

bool MC_ResMM::debug = false;
 
/* these constructors should really show use of initialization lists
   MC_ResMM::MC_ResMM() : x(5), y(10)
   {
   }
   MC_ResMM::MC_ResMM(int a, int b) : x(a), y(b)
   {
   }
*/
MC_ResMM::MC_ResMM(){

  xrnd->SetSeed(0);
  zrnd->SetSeed(0);
    
  u->SetSeed(0);
  v->SetSeed(0);

  rrnd->SetSeed(0);
  phirnd->SetSeed(0);

  setTransport = false;
    
};


void MC_ResMM::TrackDir(double& x, double& y, double& z, double& dx, double& dy, double& dz){

  if (RNDMeth==0){

    // fixed do nothing
  }
  else if (RNDMeth==1){

    // uniform distribution in x and z and direction in half-sphere
  
    x = xrnd->Uniform(Xmin,Xmax);

    z = zrnd->Uniform(Zmin,Zmax);

    y = Ypos;

    double theta = 2 * TMath::Pi() * u->Uniform(0,1);
    double phi = TMath::Pi() * v->Uniform(0,1);
    dx = sin(phi) * cos(theta);
    dy = sin(phi) * sin(theta);
    if (dy > 0) dy = -1*dy;
    dz = cos(phi);
  }
  
};



void MC_ResMM::SetChTransport(double dv, double dl, double dt, double sigmaAvlWeight){

  DriftVelo = dv;
  DLong = dl;
  DTrans = dt;

  //! The weight factor can be used as a parameter since this
  //! expression is (very) approximated.
  //!
  sigmaAvalanche = DTrans*sqrt(amplGap)*sigmaAvlWeight;

  setTransport = true;
    
};

void MC_ResMM::SetRNDRange(double xmin, double xmax, double y, double zmin, double zmax){

  Xmin = xmin;
  Xmax = xmax;
  Ypos = y;
  Zmin = zmin;
  Zmax = zmax;

};


//! parametrize electron transport based on Magboltz input values
//! I need to find out the units of the time values.
//!
void MC_ResMM::eTransport( double x0, double y0, double z0, double t0, double& xpad, double& ypad, double& zpad, double& tpad) {

  // xyzpad: x, y, z coordinate after drifting until the pad plane

  
  double sig_tr = DTrans*sqrt(z0);
  double sig_lo = DLong*sqrt(z0);

  TRandom3 *xrnd = new TRandom3;
  xrnd->SetSeed(0);

  xpad = xrnd->Gaus(x0,sig_tr);

  TRandom3 *yrnd = new TRandom3;
  yrnd->SetSeed(0);

  ypad = yrnd->Gaus(y0,sig_tr);

  TRandom3 *zrnd = new TRandom3;
  zrnd->SetSeed(0);

  zpad = zrnd->Gaus(z0,sig_lo);
  
  //! In this case the DriftVelo is negative!
  //! Hence it is best to take the absolute value in order
  //! not to have negative times.
  //! 
  tpad = t0 + fabs(zpad/DriftVelo);
  
  
};


// needed to randomize MM gain
void MC_ResMM::CalcPolyaIntegral(){

  double maxGRat = 5.;
  
  for (int i=0; i<nbingain; i++){

    RatioGain = i*maxGRat/nbingain;

    if ( i > 0){
      polyaInt[i] = polyaInt[i-1] + (PolyPara+1)/tgamma(PolyPara+1)*pow(((PolyPara+1)*RatioGain),(PolyPara))*exp(-(PolyPara+1)*RatioGain);
      
      CpolyaInt[i] = i*maxGRat/nbingain + maxGRat/2/nbingain;
    }
    else {

      polyaInt[i] = (PolyPara+1)/tgamma(PolyPara+1)*pow(((PolyPara+1)*RatioGain),(PolyPara))*exp(-(PolyPara+1)*RatioGain);
           
      CpolyaInt[i] = i*maxGRat/nbingain + maxGRat/2/nbingain;

    }
      
  }

  for (int i=0; i<nbingain; i++){

    polyaInt[i] = polyaInt[i]/polyaInt[nbingain-1];


  }
  
  
};


// return electron gain randomized
int MC_ResMM::eGain(){

  int gain = 0.;
  
  if (CalcPolyInt == 0){

    CalcPolyaIntegral();   

    CalcPolyInt = 1;
  }


  double dum = u->Uniform(0.,1.);
  //double dum = distribution(generator);
  
  for (int i=1; i<nbingain; i++){

    if ((dum > polyaInt[i-1]) && (dum < polyaInt[i])){

      gain = int(CpolyaInt[i-1]*MeanGain);
	
    }

  }

  
  return gain;

};



// used to randomize spread => to be checked
void MC_ResMM::CalcSpreadInt(double t0, double t){

  double rad_frac = 0.;

  double a = tau/(4*(t-t0));
  
  for (int i=0; i<nbinspread; i++){

    rad_frac = i*RMAX/nbinspread;

    if ( i > 0){
      SpreadInt[i] = SpreadInt[i-1] + 2*a*rad_frac*exp(-a*(rad_frac*rad_frac));
      
      CSpreadInt[i] = i*RMAX/nbinspread + RMAX/2/nbinspread;
    }
    else {

      SpreadInt[i] = 2*a*rad_frac*exp(-a*(rad_frac*rad_frac));
           
      CSpreadInt[i] = i*RMAX/nbinspread + RMAX/2/nbinspread;
       
    }
     
  }
    
  for (int i=0; i<nbinspread; i++){

    SpreadInt[i] = SpreadInt[i]/SpreadInt[nbinspread-1];


  }
  
};


// returns spread after time t => to be checked
bool MC_ResMM::eSpread(double x0, double y0, double t0, double t, double& xs, double& ys){

  double rad = 0.;

  if (t0<t){
  
    if (last_t != (t-t0)){

      CalcSpreadInt(t0, t);   

      last_t = (t-t0);
    }

   double dum = rrnd->Uniform(0.,1.);
  
    for (int i=1; i<nbinspread; i++){

      if ((dum > SpreadInt[i-1]) && (dum < SpreadInt[i])){

	//rad = CSpreadInt[i-1]*100.;
	rad = CSpreadInt[i-1];
	
      }

    }

    double phi = 2 * TMath::Pi() * phirnd->Uniform(0,1);   
    xs = x0 + rad*cos(phi);
    ys = y0 + rad*sin(phi);


    return true;
    
  }
  else {
    std::cout << "Warning: t smaller than t0" << std::endl;
    return false;
  }

  
}; 

//! The signals array has to be a nPadsX*nPadsY array.
//!
/*bool MC_ResMM::padSignals( double N, double x0, double y0, double t0, double t, double* sigs ) {
   
};*/

void MC_ResMM::angle2dir(double phi, double theta, double& dx, double& dy, double& dz){

  dy = -cos(theta);
  dz = sin(theta)*sin(phi);
  dx = sin(theta)*cos(phi);

};

void MC_ResMM::generateGrid( int npx, int npy, double lpx, double lpy ) {

  nPadsX = npx; nPadsY = npy;
  lPadX = lpx;  lPadY = lpy;

  padGridX = new TMatrixD( nPadsX + 1, nPadsY + 1 );
  padGridY = new TMatrixD( nPadsX + 1, nPadsY + 1 );
  for( int i = 0; i < nPadsX+1; i++) {
    for( int j = 0; j < nPadsY+1; j++) {
      (*padGridX)(i, j) = i*lPadX;
      (*padGridY)(i, j) = j*lPadY;
    }
  }

  if (debug) {
    std::cout << "*** " << this <<": created the following pad grid" << std::endl;
    for( int i = 0; i < nPadsX+1; i++) {
      for( int j = 0; j < nPadsY+1; j++) {
        std::cout << "(" << (*padGridX)(i, j) << ", " << (*padGridY)(i, j) << ")" << "  ";
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }

  return;
};

double MC_ResMM::getSigmaAvalanche() {
   if (setTransport == false) {
      std::cout << "*** MM Exception: run MC_ResMM::SetChTransport before accessing the sigmaAvalanche parameter! ***" << std::endl;
      throw MMExcep();
   }

   return sigmaAvalanche;
};
