/* Nota Bene
 * 1) manca identificatore di evento -> sono eventi FE... e non BE -> cambia
 *
 */
#include "evwriter.h"

#include "frame.h"
#include "datum_decoder.h"

#include "Mapping.h"
#include "DAQ.h"
#include "Pads.h"
#include "Pixel.h"
#include "T2KConstants.h"

using namespace std;

DAQ daq;
Mapping T2K;
Pads padPlane;
int Card[36][32];
int Chip[36][32];
int Chan[36][32];

/*******************************************************************************
 * Mapping and Useful functions
 * *******************************************************************************/
int padNum(int i, int j){return(j*geom::nPadx+i);}
int iFrompad(int padnum){return(padnum%geom::nPadx);}
int jFrompad(int padnum){return(padnum/geom::nPadx);}

void InitMaps() {
  // DAQ daq;
  daq.loadDAQ();
  cout << "...DAQ loaded successfully" << endl;
  // Mapping T2K;
  T2K.loadMapping();
  cout << "...Mapping loaded succesfully." << endl;
  // Pads padPlane;
  padPlane.loadPadPlane(daq, T2K);
  cout << "...Pad plane loaded succesfully." << endl;
	
  for (int i=0;i<36;i++){ for (int j=0;j<32;j++){
    Card[i][j]=-1; Chip[i][j]=-1; Chan[i][j]=-1;
  }}

  for (int car=0;car<4;car++){ for (int chi=0;chi<4;chi++){ for (int cha=3;cha<79;cha++){
  if (cha != 15 && cha != 28 && cha != 53 && cha !=66) {

  int x = T2K.i(car, chi, daq.connector(cha)); int y = T2K.j(car, chi, daq.connector(cha));
	
  if(Card[x][y] < 0) { Card[x][y]=car; }else{ printf("Card %d %d already assigned \n",x,y); }
  if(Chip[x][y] < 0) { Chip[x][y]=chi; }else{ printf("Chip %d %d already assigned \n",x,y); }
  if(Chan[x][y] < 0) { Chan[x][y]=cha; }else{ printf("Chan %d %d already assigned \n",x,y); }

  //printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);

  }}}}

  return;
}
  
void TestMaps() {
  printf("test maps \n");
  int car = 1;
  int chi = 1;
  int cha = 1;
  int x = T2K.i(car, chi, daq.connector(cha)); int y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);
  return;
}

struct tm *RunTime;
time_t tim;
int runnumber;
int subrunnumber;
char *fpname;
char pathdir[100];
char sdate[256],*date;
char srun[10],*run;
char ssubr[10],*subr;
char sdir[256],rdir[256];
char sstrg[100],*strg;
char cmd[256];
FILE *fpout;

void InitIO() {
  sprintf(pathdir,".");
  //sprintf(sdir,"%srun%.5d/",pathdir,runnumber);
  sprintf(sdir,"%s/MC",pathdir);
  sprintf(srun,"_%.5d",runnumber);
  run = strcat(srun,"");
  sprintf(ssubr,"_%.3d",subrunnumber);
  subr = strcat(ssubr,"");
  sprintf(sdate,"%.4d_%.2d_%.2d-%.2d_%.2d_%.2d",
		  (RunTime->tm_year)+1900,
		  RunTime->tm_mon+1,
		  RunTime->tm_mday,
		  RunTime->tm_hour,
		  RunTime->tm_min,
		  RunTime->tm_sec);
  date = strcat(sdate,"");

  fpname = (char *)malloc(256*sizeof(char));
  memcpy(fpname,sdir,256*sizeof(char));
//fpname = strcat(fpname,date);
  fpname = strcat(fpname,run);
  fpname = strcat(fpname,subr);
  fpname = strcat(fpname,".acq");
  printf("file name %s \n",fpname);
  return;
}

void TermIO() {
  free(fpname);
  return;
}

size_t BUFLEN=100000000;
uint8_t* buf; //dynamic (heap) 
long wbuf;

int nEvents;

uint64_t timestamp;

#define NPADMAX 1152
#define NBINMAX 512
int nPads;
int xPad[NPADMAX];
int yPad[NPADMAX];
int ADC[NPADMAX][NBINMAX];

void InitEvent(){

  timestamp = 0x0000123456789999;
  nPads = 10;

  for (int nPad=0; nPad<nPads; nPad++){
    int x = 20 + nPad; 
    int y = 10 + nPad;
    xPad[nPad] = x;
    yPad[nPad] = y;
    printf("pad # %d %d %d Card %d Chip %d Chan %d \n", nPad, x,y, Card[x][y], Chip[x][y] , Chan[x][y]);
    for (int nBin=0;nBin<NBINMAX;nBin++) {
      if (nBin >10 && nBin <100) {
        ADC[nPad][nBin] = (nBin-10);
      }
      if (nBin >150 && nBin <200) {
        ADC[nPad][nBin] = (200-nBin);
      }
    }
  }

  return;
}

/*******************************************************************************
 Main
*******************************************************************************/
int evwriter(int argc, char **argv) {

// init 
  tim = time(0);
  RunTime = (struct tm*) localtime(&tim);
  runnumber = 10000;
  nEvents = 10;

// mapping
  InitMaps();
  TestMaps();
// i/o
  InitIO();
  fpout = fopen(fpname,"w");

// dynamic buffer allocation
  buf = (uint8_t*)malloc(BUFLEN);

// new event 
  for (int nEvt=0; nEvt<nEvents; nEvt++){
    unsigned short datum;

    InitEvent();

// Buffer reset    
    wbuf = 0;
 
// event header 
    datum = 0xFFFF & (unsigned short) PFX_START_OF_EVENT;  
    memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
 
    datum = 0xFFFF & ((timestamp & 0x00000000FFFF) >> 0);  
    memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
    datum = 0xFFFF & ((timestamp & 0x0000FFFF0000) >> 16);  
    memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
    datum = 0xFFFF & ((timestamp & 0xFFFF00000000) >> 32);  
    memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
 
    datum = 0xFFFF & ((nEvt & 0x0000FFFF) >> 0);  
    memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
    datum = 0xFFFF & ((nEvt & 0xFFFF0000) >> 16);  
    memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
 
// data 
    for (int nPad=0;nPad < nPads;nPad++){
      int OT = 0;  // overthreshold status
      int OTT = 0; // transition over threshold
      int nPul = 0; // pulses over threshold
      int nDat = 0; // data count (should be even)
      unsigned short thisADC = 0;
      unsigned short prevADC = 0;
      for (int nBin=0;nBin < NBINMAX; nBin++) {
        thisADC = ADC[nPad][nBin];
        if (prevADC == 0 && thisADC > 0){
          OT = 1;
          OTT = 1;
          nPul++;
          if (nPul == 1) { // write Card/Chip/Chap (once per chan if any data >0)
            int x = xPad[nPad];
            int y = yPad[nPad];
            int nCard = Card[x][y];
            int nChip = Chip[x][y];
            int nChan = Chan[x][y];
            datum = 0xFFFF & (PFX_EXTD_CARD_CHIP_CHAN_HIT_IX);  
            memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
            nDat+=1;
            datum = 0xFFFF & (((0x1F & nCard) << 11) | ((0xF & nChip) << 7)  | (0xBF & nChan));  
            memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
            nDat+=1;
          }
          if (OTT == 1) { // write Time Bin Index 
            datum = 0xFFFF & (PFX_TIME_BIN_IX | (0x1FF & nBin));  
            memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
            nDat+=1;
            OTT = 0;
          }
          // write ADC datum
          datum = 0xFFFE & (PFX_ADC_SAMPLE | (0xFFF & thisADC));  
          memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
          nDat+=0;
          prevADC = ADC[nPad][nBin];
        } else if (prevADC > 0 && thisADC > 0){
          OT = 1;
          OTT = 0;
          // write ADC datum
          datum = 0xFFFE & (PFX_ADC_SAMPLE | (0xFFF & thisADC));  
          memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
          nDat+=0;
          prevADC = ADC[nPad][nBin];
        } else if (prevADC > 0 && thisADC == 0){
          OT = 0;
          OTT = 0;
          prevADC = ADC[nPad][nBin];
        } else {
          OT = 0;
          OTT = 0;
          prevADC = ADC[nPad][nBin];
        }
      }
      if (nDat % 2 == 1) {
        datum = 0x0;
        memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
      }
    }
  
    printf("datum %x \n",datum);
    printf("buf len %d \n",wbuf);
 
    if (fwrite(buf,wbuf,1,fpout) != 1){
      printf("Error writing to file \n");
    }

  } // nEvt loop

// term IO
  fclose(fpout);
  TermIO();

  free(buf);

  return(0);
}
