//////////////////////////////////////////////////
//             MC TPC: STORE DATA               //
// Can simulate and store more than 1 event.    //
// Needs a plotter based on mctpc.              //
//////////////////////////////////////////////////

#define DEBUG 0

#undef DRIFTVIEW
#undef EFIELDVIEW
#undef BFIELDVIEW
///////// auto def below ///////
#undef TAPPLICATION
#undef VIEW

#undef GENERATE_GAS

#undef RANDOM_TRACKS //for generating gaussian spread tracks as in Desy beam (check sigma!)

#undef FIELDUSER   
#define FIELDDESY   

#undef MANUAL_DIFFUSION //ONLY FOR DRIFT_RKF:decide whether applying "manual" diffusion or not (check D!)

#undef DRIFT_SIMPLE    
#define DRIFT_RKF   
#undef DRIFT_MC   

#undef SIGNAL_OFF

#if defined (EFIELDVIEW) || defined(BFIELDVIEW) || defined (DRIFTVIEW)
#define TAPPLICATION
#define VIEW
#endif 

// Standard C++ include files
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <algorithm>
#include <chrono> 
#include <functional>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <random>

// Root include files.
#include <TROOT.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TPaveLabel.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TRandom3.h>
#include <TFile.h>
#include <TTree.h>
#include <TObjArray.h>
#include "TAxis.h"
#include "TColor.h"
#include "TPaletteAxis.h"
#include "TSystem.h"
#include "TMath.h"
#include "TLine.h"
#include "TBox.h"

// Garfield++, Heed, Magboltz
#include "Random.hh"
#include "MediumMagboltz.hh"
#include "FundamentalConstants.hh"
#include "SolidBox.hh"
#include "GeometrySimple.hh"
#include "Sensor.hh"
#include "TrackHeed.hh"

#include "Plotting.hh"
#include "ViewMedium.hh"
#include "ViewGeometry.hh"
#include "ViewField.hh"
#include "ViewCell.hh"
#include "ViewDrift.hh"
#include "Plotting.hh"

#include "ComponentAnalyticField.hh"
#include "ComponentConstant.hh"
#include "ComponentVoxel.hh"
#include "ComponentUser.hh"
#include "Component.hh"

#include "DriftLineRKF.hh"
#include "AvalancheMC.hh"
#include "AvalancheMicroscopic.hh"

// User defined headers - MC_RMM
#include "MC_ResMM.h"

// User defined headers - event write
#include "evwriter.h"
#include "T2KConstants.h"
#include "frame.h"
#include "datum_decoder.h"
#include "Mapping.h"
#include "DAQ.h"
#include "Pads.h"
#include "Pixel.h"

using namespace std;
using namespace Garfield;
using namespace std::chrono; 

const std::string linesep = "---------------------------------------------------------------------------------------------------------";


std::random_device rd;


/*******************************************************************************
 * GEOMETRY (unit = cm)
 * *******************************************************************************/
double cdepth  = 99.25;
double pheight = 1.009; // was 0.685 // row width
double pwidth  = 1.118; // was 0.965 // column width
int nrow = 32; // was 48 // -> y // ref to n::npady in T2KConstrants file
int ncol = 36; // was 36 // -> x // ref to n::nPadx in T2KConstrants file 
double cheight = pheight * nrow + 1.0;
double cwidth  = pwidth  * ncol + 1.0;
const double mmheight = pheight * nrow;
const double mmwidth  = pwidth  * ncol;

// SubPad parameters
int subrown = 5; // sub rows in a pad  
int subcoln = 5; // sub cols in a pad
double subpheight = pheight / subrown;
double subpwidth  = pwidth  / subcoln;
int nsubrows = subrown * nrow;
int nsubcols = subcoln * ncol;
const double deltatmax = 38; // distanza temporale tra clusters massima per considerarli nella stessa sub-pad

// charge spread parametets 
const int dcol0 = -3; // charge spread from col0+dcol0 columns // def = -2
const int dcol1 = +3; // charge spread   to col0+dcol1 columns // def = +2
const int drow0 = -3; // charge spread from row0+drow0 columns // def = -2
const int drow1 = +3; // charge spread   to row0+drow1 columns // def = +2
/*******************************************************************************
 * EVENT WRITE 
 * Mapping and Useful functions
 * *******************************************************************************/
int nEvents = 5;

// memory buffer for write event dynamic allocation
const unsigned int BUFLEN = 10000000;
uint8_t* buf; //dynamic (heap) -> see malloc later 
unsigned short datum;
unsigned long wbuf;

uint64_t timestamp;

DAQ daq;
Mapping T2K;
Pads padPlane;
int Card[36][32];
int Chip[36][32];
int Chan[36][32];

int padNum(int i, int j){return(j*geom::nPadx+i);}
int iFrompad(int padnum){return(padnum%geom::nPadx);}
int jFrompad(int padnum){return(padnum/geom::nPadx);}

void InitMaps() {
  // DAQ daq;
  daq.loadDAQ();
  cout << "...DAQ loaded successfully" << endl;
  // Mapping T2K;
  T2K.loadMapping();
  cout << "...Mapping loaded succesfully." << endl;
  // Pads padPlane;
  padPlane.loadPadPlane(daq, T2K);
  cout << "...Pad plane loaded succesfully." << endl;
	
   for (int i=0;i<36;++i){ 
      for (int j=0;j<32;++j){
         Card[i][j]=-1; Chip[i][j]=-1; Chan[i][j]=-1;
      }
   }

   for (int car=0;car<4;car++){ 
      for (int chi=0;chi<4;chi++){ 
         for (int cha=3;cha<79;cha++){
            if (cha != 39 && cha != 40 && cha != 41 && cha !=42) {      //cha != 15 && cha != 28 && cha != 53 && cha !=66

            int x = T2K.i(car, chi, daq.connector(cha)); int y = T2K.j(car, chi, daq.connector(cha));
	
               if(Card[x][y] < 0) { Card[x][y]=car; }else{ printf("Card %d %d already assigned \n",x,y); 
               }
               if(Chip[x][y] < 0) { Chip[x][y]=chi; }else{ printf("Chip %d %d already assigned \n",x,y); 
               }
               if(Chan[x][y] < 0) { Chan[x][y]=cha; }else{ printf("Chan %d %d already assigned \n",x,y); 
               }

               //printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
            }
         }
      }
   }
   cout << linesep << endl;

   return;
}
  
void TestMaps() {
  printf("test maps \n");
  int car = 1;
  int chi = 2;
  int cha =7;
  int x = T2K.i(car, chi, daq.connector(cha)); 
  int y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);

  cout << linesep << endl;
  printf("test maps \n");
   car = 1;
   chi = 2;
   cha =8;
   x = T2K.i(car, chi, daq.connector(cha)); 
   y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);

  cout << linesep << endl;
  printf("test maps \n");
   car = 2;
   chi = 2;
   cha =7;
   x = T2K.i(car, chi, daq.connector(cha)); 
   y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);

  cout << linesep << endl;
  printf("test maps \n");
   car = 2;
   chi = 2;
   cha =8;
   x = T2K.i(car, chi, daq.connector(cha)); 
   y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);

  cout << linesep << endl;
  printf("test maps \n");
   car = 3;
   chi = 3;
   cha =26;
   x = T2K.i(car, chi, daq.connector(cha)); 
   y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);

  cout << linesep << endl;
  printf("test maps \n");
   car = 3;
   chi = 0;
   cha =4;
   x = T2K.i(car, chi, daq.connector(cha)); 
   y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);

  cout << linesep << endl;
  printf("test maps \n");
   car = 0;
   chi = 2;
   cha =7;
   x = T2K.i(car, chi, daq.connector(cha)); 
   y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);

  cout << linesep << endl;
  printf("test maps \n");
   car = 0;
   chi = 2;
   cha =8;
   x = T2K.i(car, chi, daq.connector(cha)); 
   y = T2K.j(car, chi, daq.connector(cha));
  printf(" Card %d, Chip %d, Chan %d -> pad (%d,%d)\n",car, chi, cha, x,y);
  printf(" pad (%d, %d) -> Card %d, Chip %d, Chan %d \n",x,y, Card[x][y], Chip[x][y], Chan[x][y]);

  cout << linesep << endl;

  return;
}

struct tm *RunTime;
time_t tim;
int runnumber = 10000;
int subrunnumber;
char *fpname;
char pathdir[100];
char sdate[256],*date;
char srun[10],*run;
char ssubr[10],*subr;
char sdir[256],rdir[256];
char sstrg[100],*strg;
char cmd[256];
FILE *fpout;

void InitIO() {
  sprintf(pathdir,".");
  //sprintf(sdir,"%srun%.5d/",pathdir,runnumber);
  sprintf(sdir,"%s/MC",pathdir);
  sprintf(srun,"_%.5d",runnumber);
  run = strcat(srun,"");
  sprintf(ssubr,"_%.3d",subrunnumber);
  subr = strcat(ssubr,"");
  sprintf(sdate,"%.4d_%.2d_%.2d-%.2d_%.2d_%.2d",
		  (RunTime->tm_year)+1900,
		  RunTime->tm_mon+1,
		  RunTime->tm_mday,
		  RunTime->tm_hour,
		  RunTime->tm_min,
		  RunTime->tm_sec);
  date = strcat(sdate,"");

  fpname = (char *)malloc(256*sizeof(char));
  memcpy(fpname,sdir,256*sizeof(char));
//fpname = strcat(fpname,date);
  fpname = strcat(fpname,run);
  fpname = strcat(fpname,subr);
  fpname = strcat(fpname,".aqs");
  printf("file name %s \n",fpname);
  return;
}

void TermIO() {
  free(fpname);
  return;
}

#define NPADMAX 1152
#define NBINMAX 512
/*
int nPads;
int xPad[NPADMAX];
int yPad[NPADMAX];
int ADC[NPADMAX][NBINMAX];

void TestEventWritePattern(){
  timestamp = 0x0000123456789999;
  nPads = 10;

  for (int nPad=0; nPad<nPads; nPad++){
    int x = 20 + nPad; 
    int y = 10 + nPad;
    xPad[nPad] = x;
    yPad[nPad] = y;
    printf("pad # %d %d %d Card %d Chip %d Chan %d \n", nPad, x,y, Card[x][y], Chip[x][y] , Chan[x][y]);
    for (int nBin=0;nBin<NBINMAX;nBin++) {
      if (nBin >10 && nBin <100) {
        ADC[nPad][nBin] = (nBin-10);
      }
      if (nBin >150 && nBin <200) {
        ADC[nPad][nBin] = (200-nBin);
      }
    }
  }

  return;
}
*/

void InitEvent(){

  return;
}

void styleHist(TGraph2D* g, string title, string xTitle, string yTitle, string zTitle) {
    g->GetXaxis()->SetTitle( xTitle.c_str() );
    g->GetXaxis()->SetLabelSize(0.02);
    g->GetXaxis()->SetTitleSize(0.03);
    g->GetYaxis()->SetTitle( yTitle.c_str() );
    g->GetYaxis()->SetLabelSize(0.02);
    g->GetYaxis()->SetTitleSize(0.03);
    g->GetZaxis()->SetTitle( zTitle.c_str() );
    g->GetZaxis()->SetLabelSize(0.02);
    g->GetZaxis()->SetTitleSize(0.03);
    g->SetTitle( title.c_str() );
}
/*******************************************************************************
 * GARFIELD++ and PHYSICS 
 * *******************************************************************************/
// some settings
double xmin = cwidth/4.;
double xmax = 3.*cwidth/4.;
double ypos = cheight;
double zmin = cdepth/4.;
double zmax = 3.*cdepth/4.;

/*
double dx,dz = 0.;
double dy = -1.;
double x = cwidth  * 1.0;
double y = cheight * 1.0;
double z = cdepth  * 0.1;
double t = 0.;
*/

int setRNDM = 0;

// init track params  TUTTO IN cm

double xt = cwidth  * 1.0;
double yt = ( cheight * 1.0 - 20.625 ) ; //(cheight * 1.0 - 20.625) è il centro della TPC,cioè l'asse del solenoide
double z_Desy=560;//z in the desy setup in mm (number used in logbook)
double zt = 99.25 - 9.015 - z_Desy/10.;
double tt = 0.;
double dxt  = -1.0; // coseni direttori
double dyt  =  0.0;
double dzt  =  0.0;
double phit =  0.0; //15./180.*3.14
double thet =  0.0; // 5./180.*3.14

// avalanche default values
double vx, vy, vz = 0.;
double dl, dt = 0.;
double gain = 1000.;
double PolynaPara = 2.3;

/*******************************************************************************
 * FEE parameters
 * *******************************************************************************/
double AGain  = 0.0001; // ampliture scaling factor

/*******************************************************************************
 * TIME SAMPLING (unit = ns)
 * *******************************************************************************/
const double MCClockT  = 10.0; // MC main Clock = sampling (predigitization) clock T in ns
const int    MCSamples = 1000; // 1000 x 10ns = 10us 
const int    ClockRatio = 2;   // MC clock to sampling clock ration 
const double ADCClockT  = ClockRatio * MCClockT; // sampling (digit) clock T -> ns
const int    ADCSamples = 512; 

//const double RC = 70 * 100 * MCClockT; // ns/cm2 - nota: ordine di grandezza OK e` 70ns/mm2 = 7000 ns/cm2
const double RC = 700 * MCClockT;    // in ns/cm2 - nota: ordine di grandezza OK e` 70ns/mm2
const double kk = 1. / RC;           // in cm2/ns
const double taus = 206 / MCClockT; // in Clock Samples shaping time = peaking time / 2 
                                    // -- peaking time being the parameter set in FEE be = 200, 412, ...
/*******************************************************************************
 * DIGITAL FILTER - single pole HP + 6 pole LP + PZ
 * *******************************************************************************/
const double dfpz =  5; // samples (MCClockt) // for q was 900
const double dftc = 10; // samples (MCClockT) // for q was 15
/* *******************************************************************************/
const double dfb1 = exp( -1.0 / dftc);
const double dfa0 = +(1.0 + dfb1) / 2.0;   
const double dfa1 = -(1.0 + dfb1) / 2.0; 
const double dfa2 = +(1.0 - dfb1); 
const int    dfnp = 6;
const double dfgain = 6.22575; //Gain=1/(n_poles**n_poles*exp(-n_poles)/n_poles!)

/*******************************************************************************
 * USER E and B FIELD DEFINITION
 * *******************************************************************************/

// Fields
double cathVolt = 26700.;
double efield = -cathVolt/cdepth;
//double efield = 275;
double bfield = 0.2;
const double hcheight = cheight/2;
const double hcwidth  = cwidth/2;
const double hcdepth  = cdepth/2;
const double ang = 0.1/hcdepth;

auto uefield = [] (const double x, const double y, const double z, double& ex, double& ey, double& ez) {

  ex = 0.0;
  ey = 0.0;
  //ey = 10.0 * (x-cwidth/2)/cwidth;
  ez = efield;

  /*
  if (x > hcwidth) {
    if ( z <= hcdepth) {
      //ey =   efield * ang * (z/49.)      * ((21.-y)/21.);
      ey = efield * ang * (z/hcdepth);
    } else {
      //ey = - efield * ang * ((98-z)/49.) * ((21.-y)/21.);
      ey = efield * ang * ((cdepth-z)/hcdepth);
    }
  }
  */

  //double dx = std::min((cwidth-x),x);  if ( dx<5.0 ) ex = dx * 10;
  //double dy = std::min((cheight-y),y); if ( dy<5.0 ) ey = dy * 10;

};

auto ubfield = [] (const double x, const double y, const double z, double& ex, double& ey, double& ez) {
  ex = 0.0;
  ey = 0.0;
  //ex = 0.02 * (x-cwidth/2)/cwidth;
  //ey = 0.02 * (y-cheight/2)/cheight;
  ez = bfield;

};

/*******************************************************************************
 * MAIN
 * *******************************************************************************/
int main(int argc,char **argv) {

   // input parameters
   cout << linesep << endl;
   cout << "argc = " << argc << " argv[0] = " << argv[0] << endl;
   cout << linesep << endl;

#ifdef TAPPLICATION
   //TApplication app("app", &argc, argv);
   
   // need to create a ROOT application to draw a canvas, it does not need to be run
   TApplication* app = new TApplication("app", new int(0), new char* );
   app->SetReturnFromRun( kFALSE );
   if (gROOT->IsBatch()) { gROOT->SetBatch( kFALSE ); }
#endif

   // Garfield related variables 
   int status = 0;
   // Cluster coordinates
   double xc = 0., yc = 0., zc = 0., tc = 0.;
   double xc0 = 0., yc0 = 0., zc0 = 0., tc0 = 0.;
   double xp = 0., yp = 0., zp = 0., tp = 0.;
   double tlen = 0.;
   double xe1 = 0., ye1 = 0., ze1 = 0., te1 = 0.;
   double xe2 = 0., ye2 = 0., ze2 = 0., te2 = 0.;

   // Number of electrons produced in a collision
   int nc = 0;
   int netot = 0;
   // Energy loss in a collision
   double ec = 0.;
   // Dummy variable (not used at present)
   double extra = 0.;
   // Total energy loss along the track
   double esum = 0.;
   
   double xe, ye, ze, te;
   // Energy and direction of the electron (these are not provided by Heed, 
   // but the parameters are included for compatibility with other Track classes). 
   double ee, dxe, dye, dze;

   float x1ipad;
   float x0ipad;
   float y1ipad;
   float y0ipad;

   // simple drift - by hand...
   MC_ResMM mc;

   vector<int> subpadn;
   vector<int>::iterator itr;
   vector<int>::iterator pitr;
   vector<double> subpadt;
   vector<double> subpadq;
   vector<int> subpadm;
   vector<int> t0d;
   vector<int> padn;               // vettore con le pads attive di cui la forma d'onda in padwf
   vector<int> padp;               // vettore con le pads attive primarie 
   vector<vector<double>> padwf;   // vettore con tutte le forme d'onda dell'evento (vettore di vettore)
   vector<vector<double>> padwfi;  // forme d'onda derivate
   vector<vector<double>> padwfdf; // digital filtered 
   vector<vector<double>> padwfcv; // padwfi convoluted
   vector<double> padwfMax;
   vector<double> padwfMin; 
   vector<double> wf (MCSamples);
   vector<double> wf1 (MCSamples);
   vector<double> adc (ADCSamples);
   vector<double>::iterator dit;

   // debug TTree 
   Int_t     nEvtId;
   const Int_t nMaxClu = 10000;
   Int_t     nClu;
   Double_t  xClu[nMaxClu]; Double_t  yClu[nMaxClu]; Double_t  zClu[nMaxClu]; Double_t  tClu[nMaxClu]; 
   Double_t  eClu[nMaxClu]; 
   Int_t    neClu[nMaxClu];
   
   const Int_t nMaxEle = 10000;
   Int_t     nEle;
   Double_t  xEle[nMaxEle]; Double_t  yEle[nMaxEle]; Double_t  zEle[nMaxEle]; Double_t  tEle[nMaxEle];
   Double_t xpEle[nMaxEle]; Double_t ypEle[nMaxEle]; Double_t zpEle[nMaxEle]; Double_t tpEle[nMaxEle];
   Double_t  gEle[nMaxEle];

   // sub pads activated
   const Int_t nMaxSub = 10000;
   Int_t     nSub;
   Double_t  xSub[nMaxSub]; Double_t  ySub[nMaxSub]; Double_t  qSub[nMaxSub]; Double_t  tSub[nMaxSub];

   // pad activated  
   const Int_t nMaxPad = 10000; 
   Int_t     nPad;
   double_t  xPad[nMaxPad]; Double_t  yPad[nMaxPad]; Double_t  qPad[nMaxPad]; Double_t  tPad[nMaxPad];

   ////////////////////////////////////////////////////////////////
     
   TFile froot("tpcmc.root","recreate");

   TTree *t3 = new TTree("t3","tpcmc ntuple");
   t3->Branch("id",&nEvtId,"id/I");
   t3->Branch("nc",&nClu,"nc/I");
   t3->Branch("xc",xClu,"xc[nc]/D");
   t3->Branch("yc",yClu,"yc[nc]/D");
   t3->Branch("zc",zClu,"zc[nc]/D");
   t3->Branch("tc",tClu,"tc[nc]/D");
   t3->Branch("ec",eClu,"ec[nc]/D");
   t3->Branch("nec",neClu,"nec[nc]/I");
   t3->Branch("ne",&nEle,"ne/I");
   t3->Branch("xe",xEle,"xe[ne]/D");
   t3->Branch("ye",yEle,"ye[ne]/D");
   t3->Branch("ze",zEle,"ze[ne]/D");
   t3->Branch("te",tEle,"te[ne]/D");
   t3->Branch("xp",xpEle,"xp[ne]/D");
   t3->Branch("yp",ypEle,"yp[ne]/D");
   t3->Branch("zp",zpEle,"zp[ne]/D");
   t3->Branch("tp",tpEle,"tp[ne]/D");
   t3->Branch("ge",gEle,"ge[ne]/D");
   t3->Branch("ns",&nSub,"ns/I");
   t3->Branch("xs",xSub,"xs[ns]/D");
   t3->Branch("ys",ySub,"ys[ns]/D");
   t3->Branch("qs",qSub,"qs[ns]/D");
   t3->Branch("ts",tSub,"ts[ns]/D");
   t3->Branch("npd",&nPad,"npd/I");
   t3->Branch("xpd",xPad,"xpd[ns]/D");
   t3->Branch("ypd",yPad,"ypd[ns]/D");
   t3->Branch("qpd",qPad,"qpd[ns]/D");
   t3->Branch("tpd",tPad,"tpd[ns]/D");
   t3->Branch("padwf",&padwf);
   t3->Branch("padwfi",&padwfi);
   t3->Branch("padwfdf",&padwfdf);
   t3->Branch("padwfcv",&padwfcv);
   t3->Branch("padn",&padn);
   t3->Branch("padp",&padp);
   //t3->Branch("adc",&adc);
   
   // init time and file info 
   tim = time(0);
   RunTime = (struct tm*) localtime(&tim);
   // init mapping
   InitMaps();
   TestMaps();
   // init i/o & file open 
   InitIO();
   fpout = fopen(fpname,"w");

   // init dynamic buffer allocation
   buf = (uint8_t*)malloc(BUFLEN); //controlla il free !!!

   // exec time start
   auto start = high_resolution_clock::now();
   /////////////////////////////////////////////

   // exec time stop -> show
   auto stop = high_resolution_clock::now(); 
   auto duration = duration_cast<microseconds>(stop - start);
   cout << "Exec Time - MC_resMM init = " << duration.count() << endl;
   cout << linesep << endl;
   /////////////////////////////////////////////

   //mc.generateGrid( nrow, ncol, pheight, pwidth );
   //const TMatrixD& gridX = mc.getPadGridX();
   //const TMatrixD& gridY = mc.getPadGridY();

   vector<double> xtr, ytr, ztr, ttr;
   vector<double> xtrp, ytrp, ztrp, ttrp;
   vector<int> nCharges;

   //char histName[10];

   //char gasf[256] = "gas_files/T2K_ExBxA_5x5x5_293K_760Torr.gas";
   char gasf[256] = "gas_files/T2K_ExBxA_6x6x6_293K_760Torr.gas";

   // Setup a Medium
#ifndef GENERATE_GAS
   Garfield::MediumMagboltz* gas = new Garfield::MediumMagboltz();
   gas->LoadGasFile(gasf); 
   gas->SetTemperature(293.15);
   gas->SetPressure(760.00);

   cout << "Medium (pointer) = " << gas << endl;
#ifdef VIEW
   Garfield::ViewMedium mediumView;
   mediumView.SetMedium(gas);
   mediumView.PlotElectronVelocity('e');
#endif
#endif

   //gas->SetPressure(380.);
   //cout << "\033[1;31m" << "attenzione -> gas H2 @ 380Torr" << "\033[0m" << endl;

   // generate gas file (meglio usare file tpcgas)
#ifdef GENERATE_GAS
   Garfield::MediumMagboltz* gas = new Garfield::MediumMagboltz();
   gas->SetComposition("Ar", 95., "CF4", 3., "iC4H10", 2.);
   gas->SetTemperature(293.15);
   gas->SetPressure(760.00);
   // Set the field range to be covered by the gas table.
   const int neFields = 5;
   const double emin =     10.;
   const double emax =    275.;
   constexpr bool useLog = true;
   // Flag to request logarithmic spacing.
   const int nbFields = 4;
   const double bmin =     0.0;
   const double bmax =     0.3;
   // Flag to request logarithmic spacing.
   const int nAngles  = 5;
   const double amin =     0.0;
   const double amax =     1.5;
   gas->SetFieldGrid(emin, emax, neFields, useLog, bmin, bmax, nbFields, amin, amax, nAngles);
   const int ncoll = 10;
   // Run Magboltz to generate the gas table.
   gas->GenerateGasTable(ncoll);
   // Save the table.
   gas->WriteGasFile("T2K.gas");

   return 0;
#endif

   if (! gas->ElectronVelocity(0.,0.,efield,0.,0.,bfield, vx, vy, vz)) 
      cout << "\033[1;31m" << "WARNING: no electron velocity " << endl;
   if (! gas->ElectronDiffusion(0.,0.,efield,0.,0.,bfield, dl, dt))
      cout << "\033[1;31m" << "WARNING: no electron diffusion " << endl;

   mc.SetChTransport(vz, dl, dt);

   // Setup Detector geometry
   // 
   // The arguments of SolidBox are "centers" and "halflengths".
   // Garfield::SolidBox* box = new Garfield::SolidBox(cwidth, cheight, cdepth/2, cwidth, cheight, cdepth/2); // was wrong up to 20210810
   Garfield::SolidBox* box = new Garfield::SolidBox(cwidth/2, cheight/2, cdepth/2, cwidth/2, cheight/2, cdepth/2);
   cout << "Compare the following with the Garfield++ debug output:" << endl;
   cout << "cwidth: " << cwidth << "; cheight: " << cheight << "; cdepth: " << cdepth << ";" << endl;
   cout << "(From Garfield++, half-lengths X,Y,Z: " << box->GetHalfLengthX() << ", " <<  box->GetHalfLengthY() << ", " << box->GetHalfLengthZ() << ")" << endl;

   Garfield::GeometrySimple* geo = new Garfield::GeometrySimple();
   geo->AddSolid(box, gas);

   // Build Sensor
   Sensor* sensor = new Sensor();

   // Build component Fields 
#if defined (FIELDUSER)
   cout << "FIELD USER OPTION ENABLED" << endl;
   ComponentUser* comp = new ComponentUser();
   comp->SetGeometry(geo);
   comp->SetElectricField(uefield);
   comp->SetMagneticField(ubfield);
   sensor->AddComponent(comp);
#elif defined (FIELDDESY)
   cout << "FIELD DESY OPTION ENABLED" << endl;

   //////////////////////////////
   // User component for Efield
   //////////////////////////////
   // sembra che funzioni ugualmente ! anche se da molti errori !!!
   ComponentUser* compe = new ComponentUser();
   compe->SetGeometry(geo); // se usi voxel non devi settare qui la geometria !!! -> altrimenti errore "no interpolation for E field"
   compe->SetElectricField(uefield);
   //ComponentConstant* compe = new ComponentConstant();
   //compe->SetGeometry(geo);
   //compe->SetElectricField(0., 0., efield);

   //////////////////////////////
   // Voxel component for Bfield
   //////////////////////////////
   // per generare fine bfield vedere -> /home/collazuo/wa/ilcsoft/README-GC
   /////////////////////////////////////////////////////////////////////////////////
   // parameters as of file 
   // /home/collazuo/wa/ilcsoft/marlintpc/tools/processors/src/DrawMagneticFieldProcessor.cc
   //const int xBi = 100;
   //const int yBi = 100;
   //const int zBi = 100;
   //const double xMi = -250.; 
   //const double xMa = 250.; 
   //const double yMi = -250.; 
   //const double yMa = 250.; 
   //const double zMi = -550.; 
   //const double zMa = 550.; 
   //const std::string bfield_file_name = "bfield_files/bfield-0_0_0_0_0-100_100_100_250_250_550.txt";

   //const int xBi = 200;
   //const int yBi = 200;
   //const int zBi = 200;
   //const double xMi = 0; 
   //const double xMa = cwidth; 
   //const double yMi = 0; 
   //const double yMa = cheight; 
   //const double zMi = 0; 
   //const double zMa = cdepth; 
   //const std::string bfield_file_name = "bfield_files/bfield__206.25_126.65_498.25__200_200_200_-50_470_-50_470_-50_1050.txt";

   // ??? problemi ??? 
   // 1) precisione -> doppi bins !!!
   // 2) no voxel per E field -> dove nasce problema ? -> si puo` dare voxel per E field = 0 (dove somma componenti ???)
   // 3) ... mesh dovrebbe essere indipendente da file... invece si aspetta che sia tutta riempita !!! ne piu` ne meno ... -> check
   unsigned int xBi = 100; // NOTA BENE !!! vanno messi nBin in generazione +! ///
   unsigned int yBi = 100;
   unsigned int zBi = 100;
   double xMi =  -5.0; // Attenzione -> coordinate in reference TPC (ed in mm)
   double xMa =  47.0; // in mm
   double yMi =  -5.0; // in mm
   double yMa =  47.0; // in mm
   double zMi =  -5.0; // in mm
   double zMa = 105.0; // in mm
   xMi -= (xMa-xMi)/xBi/1000.0;
   xMa += (xMa-xMi)/xBi/1000.0;
   yMi -= (yMa-yMi)/yBi/1000.0;
   yMa += (yMa-yMi)/yBi/1000.0;
   zMi -= (zMa-zMi)/zBi/1000.0;
   zMa += (zMa-zMi)/zBi/1000.0;
   //++xBi;
   //++yBi;
   //++zBi;
   //const std::string bfield_file_name = "bfield_files/bfield__206.25_126.65_498.25__100_100_100_-50_470_-50_470_-50_1050.txt";
   //const std::string bfield_file_name = "bfield_files//bfield_negative_Bz_206.25_126.65_498.25__100_100_100_-50_470_-50_470_-50_1050.txt";
   //const std::string bfield_file_name = "bfield_files//bfield_negative_Br_206.25_126.65_498.25__100_100_100_-50_470_-50_470_-50_1050.txt";
   const std::string bfield_file_name = "bfield_files/bfield__206.25_126.65_498.25__200_200_200_-50_470_-50_470_-50_1050.txt";
   
   // bfield file generated in 
   // /home/collazuo/wa/ilcsoft/marlintpc/examples/draw_MagneticField
   // with command 
   // > /home/collazuo/wa/ilcsoft/marlin/bin/Marlin draw_MagneticField.xml
   // edit /home/collazuo/wa/ilcsoft/marlintpc/tools/processors/src/DrawMagneticFieldProcessor.cc
   // cd /home/collazuo/wa/ilcsoft/marlintpc/build; make -j8 

   ComponentVoxel* compb = new ComponentVoxel();
   compb->SetMesh(xBi, yBi, zBi, xMi, xMa, yMi, yMa, zMi, zMa);
   compb->LoadMagneticField(bfield_file_name, "XYZ", 1.0, 0.2); // scaling factors: 0.2 = 1T -> 0.2T field
   //compb->EnablePeriodicityX();
   //compb->SetMedium(0, si);
   compb->PrintRegions();
   compb->EnableInterpolation();
   //compb->SetGeometry(geo);
   //
   sensor->AddComponent(compb); 
   sensor->AddComponent(compe);

#else // Constant
   cout << "FIELD SIMPLE OPTION ENABLED" << endl;
   ComponentConstant* comp = new ComponentConstant();
   comp->SetGeometry(geo);
   comp->SetElectricField(0., 0., efield);
   comp->SetMagneticField(0., 0., bfield);
   sensor->AddComponent(comp);
#endif

   sensor->SetArea(0.,0.,0.,cwidth,cheight,cdepth);

   // Debug Sensor fields & parameters
   { double x,y,z,fx,fy,fz,fv,fbx,fby,fbz,fvx,fvy,fvz,fdl,fdt;
   Garfield::Medium* medium;

   //x = y = z = 10;
   x = 14;
   y = 11;
   z = -40;

   status = 0;
   sensor->ElectricField(x,y,z,fx,fy,fz,fv,medium,status);
   cout << "Electric Field debug " << endl;
   cout << "x,y,z " << x << " " << y << " " << z << endl;
   cout << "ex,ey,ez,medium,status " << fx << " " << fy << " " << fz << " " << medium << " " << status << endl;

   status = 0;
   cout << "Magnetic Field debug " << endl;
   for(int i=0;i<1;i++){
      z=z+1;
      sensor->MagneticField(x,y,z,fbx,fby,fbz,status);
      cout << "x,y,z " << x << " " << y << " " << z << endl;
      cout << "bx,by,bz,status " << fbx << " " << fby << " " << fbz << " " << status << endl;
    }
   cout << "Electron Velocity & Diffusion OK ? " <<
           gas->ElectronVelocity(fx, fy, fz, fbx, fby, fbz, fvx, fvy, fvz) << " & " <<
           gas->ElectronDiffusion(fx, fy , fz, fbx, fby, fbz, fdl, fdt) << " " <<
           " vx " << fvx << " vy " << fvy << " vz " << fvz << " Dl " << fdl << " Dt " << fdt << endl;
   }

   // Setup DRIFT_SIMPLE
#ifdef DRIFT_SIMPLE
   if (! gas->ElectronVelocity(0.,0.,efield,0.,0.,bfield, vx, vy, vz)) 
      cout << "\033[1;31m" << "WARNING: no electron velocity " << endl;
   if (! gas->ElectronDiffusion(0.,0.,efield,0.,0.,bfield, dl, dt))
      cout << "\033[1;31m" << "WARNING: no electron diffusion " << endl;
   mc.SetChTransport(vz, dl, dt);
#endif

   // Setup drift integration
#ifdef DRIFT_RKF
   DriftLineRKF* driftRKF = new DriftLineRKF();
   driftRKF->SetSensor(sensor);
#endif

   // Setup drift MC
#ifdef DRIFT_MC
   AvalancheMC* driftMC = new AvalancheMC();
   driftMC->SetSensor(sensor);
   driftMC->SetDistanceSteps(0.01);
   driftMC->EnableMagneticField()
#endif 

   // Track class
   TrackHeed* track = new TrackHeed();
   track->SetSensor(sensor);
   track->EnableMagneticField();
   track->EnableElectricField();
   track->EnableDeltaElectronTransport();
   track->SetSteppingLimits(100., 1000., 0.1, 0.01);
 
#ifdef DRIFTVIEW
   // Construct a viewer to visualise the drift lines.
   ViewDrift* driftView = new ViewDrift();
   // view track
   track->EnablePlotting(driftView);
   // view driflines
   if (true) driftRKF->EnablePlotting(driftView);
   //if (false) driftMC->EnablePlotting(driftView);
#endif
 
#ifdef EFIELDVIEW
  ViewField *view = new ViewField();
  view->SetSensor(sensor);
  view->SetElectricFieldRange(0.0, 200000.0);
  view->PlotContour("e");
#endif

#ifdef BFIELDVIEW
  TGraph2D* graphBr = new TGraph2D();
  TGraph2D* graphBz = new TGraph2D();
  TGraph2D* graphBr_xy = new TGraph2D();


  const int _xBins = 200;
  const int _zBins = 200;
  const int _yBins = 200;

  const double _xMin = -10.0;
  const double _xMax = +50.0;
  const double _zMin = -10.0;
  const double _zMax = +110.0;
  const double _yMin = -10.0;
  const double _yMax = +50.0;
  Garfield::Medium* medium;
  status = 0;
  int counter = 0;
  for (int xBin = 0; xBin < _xBins; ++xBin) {
  for (int zBin = 0; zBin < _zBins; ++zBin) {
    double x,y,z,fx,fy,fz,fv,fbx,fby,fbz,fbr;
    // calculate the position
    x = xBin*(_xMax-_xMin)/_xBins+_xMin;
    y = 0;
    z = zBin*(_zMax-_zMin)/_zBins+_zMin;
    // get the magnetic field at the position
    // for E field would be -> sensor->ElectricField(x,y,z,fx,fy,fz,fv,medium,status);
    sensor->MagneticField(x,y,z,fbx,fby,fbz,status);
    fbr = sqrt(fbx*fbx + fby*fby);
    double mod=sqrt(x*x + y*y);
    double cos=1;
    if(sqrt(fbx*fbx + fby*fby)!=0 && sqrt(x*x + y*y)!=0){
      cos = (x*fbx+y*fby)/sqrt(fbx*fbx + fby*fby)/mod;
    }
    double sgn=1;
    if(cos<0){sgn=-1;}else{sgn=1;}
    fbr = fbr*sgn;
    //cout << x << " " << y << " " << z << " " << fbr << " " << fbz << endl;        
    graphBr->SetPoint( counter, x, z, fbr );
    graphBz->SetPoint( counter, x, z, fbz );
    ++counter;
  }
  }
  counter=0;
  for (int xBin = 0; xBin < _xBins; ++xBin) {
  for (int yBin = 0; yBin < _yBins; ++yBin) {
    double x,y,z,fx,fy,fz,fv,fbx,fby,fbz,fbr;
    // calculate the position
    x = xBin*(_xMax-_xMin)/_xBins+_xMin;
    z = 99;
    y = yBin*(_yMax-_yMin)/_yBins+_yMin;
    // get the magnetic field at the position
    // for E field would be -> sensor->ElectricField(x,y,z,fx,fy,fz,fv,medium,status);
    sensor->MagneticField(x,y,z,fbx,fby,fbz,status);
    double _x=x;
    double _y=y;
    double mod=sqrt(_x*_x + _y*_y);
    double cos=1;
    if(sqrt(fbx*fbx + fby*fby)!=0 && sqrt(_x*_x + _y*_y)!=0){
      cos = (_x*fbx+_y*fby)/sqrt(fbx*fbx + fby*fby)/mod;
    }
    double sgn=1;
    if(cos<0){sgn=-1;}else{sgn=1;}
    fbr = sqrt(fbx*fbx + fby*fby)*sgn;
    //cout << x << " " << y << " " << z << " " << fbr << " " << fbz << endl;        
    graphBr_xy->SetPoint( counter, x, y, fbr );
    
    ++counter;
  }
  }


  // some styling to axis and a nicer color palette
  gROOT->SetStyle( "Plain" );
  gStyle->SetPalette( 1 );
  const Int_t NRGBs = 5;
  const Int_t NCont = 99;
  Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
  styleHist(graphBr, "B_{r} [T]", "x [mm]", "z [mm]", "B_{r} [T]");
  styleHist(graphBz, "B_{z} [T]", "x [mm]", "z [mm]", "B_{z} [T]");

  // draw a canvas
  TCanvas* canv = new TCanvas( "Magnetic Field Map", "Magnetic Field Map", 1700, 700 );
  canv->Divide( 3, 1 );
  canv->GetPad(1)->SetRightMargin(0.16);
  canv->GetPad(1)->SetLeftMargin(0.08);
  canv->GetPad(2)->SetRightMargin(0.14);
  canv->GetPad(2)->SetLeftMargin(0.08);
  canv->GetPad(3)->SetRightMargin(0.17);
  canv->GetPad(3)->SetLeftMargin(0.08);
  canv->GetPad(3)->SetTopMargin(0.16);
  canv->GetPad(3)->SetBottomMargin(0.16);

  
  TBox *box1 = new TBox(-20./26.*0.5 , -50./50.*0.48 , 21.6/26.*0.5 , 49.25/50.*0.48); 
  TBox *box2 = new TBox(-20./26.*0.5 , -50./50.*0.38 , 21.6/26.*0.5 , 49.25/50.*0.24); 


  canv->cd(1);
  graphBr->Draw( "cont4z" );
  //gPad->Range(_xMin,_zMin,_xMax,_zMax);
  //TLine line = TLine(0,0,0,cwidth); line.SetLineColor(kYellow); line.SetLineWidth(2); line.Draw();
  box1->SetLineColor(kBlack); 
  box1->SetLineWidth(5); 
  box1->SetFillStyle(0);
  box1->Draw();
  gPad->Update();

  canv->cd(2);
  gPad->SetFillStyle(0);
  graphBz->Draw( "cont4z" );
  box1->SetLineColor(kBlack); 
  box1->SetLineWidth(5); 
  box1->SetFillStyle(0);
  box1->Draw();
  gPad->Update();
  gPad->Update();

    
  canv->cd(3);
  gPad->SetFillStyle(0);
  graphBr_xy->Draw( "cont4z" );
  box2->SetLineColor(kBlack); 
  box2->SetLineWidth(5); 
  box2->SetFillStyle(0);
  box2->Draw();
  gPad->Update();
#endif
  cout << linesep << endl;

#if !defined(VIEW) 
   // loop over events
   for (int nEvt = 0; nEvt<nEvents; ++nEvt){
      nEvtId = nEvt;

      cout << "**************************** \n" << "* Event # " << nEvt << " \n" << "**************************** \n"; 

      // init event
      InitEvent();

      xtr.clear();
      ytr.clear();
      ztr.clear();
      ttr.clear();
      xtrp.clear();
      ytrp.clear();
      ztrp.clear();
      ttrp.clear();
      nCharges.clear();

      //mc.angle2dir(phit, thet, dxt, dyt, dzt); // if angles are defined  
      //mc.TrackDir(xt, yt, zt, dxt, dyt, dzt); // either init or rndm  according to RNDMeth
      //cout << "mc.trackdir " << xt << ":" << yt << ":" << zt << ":" << dxt << ":" << dyt << ":" << dzt << endl;
  
      //track->SetParticle("mu-");
      //track->SetMomentum(1.e8); // eV
      //track->SetParticle("electron");
      //track->SetMomentum(1.e8);
      track->SetParticle("e-");
      track->SetMomentum(4.e9);


#ifdef RANDOM_TRACKS
      // randomization of track starting point and angle
      
      std::mt19937 mersennetwister(rd());
      double _mean=0;
      double _sigma=0.05;//cm
      std::normal_distribution<> Gaus(_mean, _sigma);
      std::uniform_real_distribution<> Uniform(0,2*TMath::Pi());
      yt = yt + Gaus(mersennetwister);
      zt = zt+ Gaus(mersennetwister);
      

      dxt  = dxt ; // coseni direttori: sommare piccoli angoli random
      dyt  = dyt ;
      dzt  = dzt ;
#endif

      // Initial position and direction
      track->NewTrack(xt, yt, zt, tt, dxt, dyt, dzt);

      //cout << "event "<<nEvt <<" yt="<<yt<<" zt="<<zt<<endl;

      // exec time start
      start = high_resolution_clock::now();
      /////////////////////////////////////////////
 
      xc = 0.; yc = 0.; zc = 0.; tc = 0.;
      xc0 = 0.; yc0 = 0.; zc0 = 0.; tlen = 0.;
      xp = 0.; yp = 0.; zp = 0.; tp = 0.;
      nc = 0;
      netot = 0;
      ec = 0.;
      extra = 0.;
      esum = 0.;
      
      nClu = 0;
      nEle = 0;
      // Loop over the clusters.
      while (track->GetCluster(xc, yc, zc, tc, nc, ec, extra)) {
	 xClu[nClu] = xc; yClu[nClu] = yc; zClu[nClu] = zc; tClu[nClu] = tc; eClu[nClu] = ec; neClu[nClu] = nc; 
	 //cout << "cluster #" << nClu << " x,y,z,t,ne " << xc << " " << yc << " " << zc << " " << tc << " " << nc << endl;
	 //cout << "(debug) #" << nClu << " x,y,z,t,ne " << xClu[nClu] << " " << yClu[nClu] << " " << zClu[nClu] << " " << tClu[nClu] << " " << neClu[nClu] << endl;
	 if (nClu<nMaxClu) nClu++;
	 else cout << " nClu => nMaxClu" << endl;
	 tlen += sqrt((xc-xc0)*(xc-xc0)+(yc-yc0)*(yc-yc0)+(zc-zc0)*(zc-zc0));
	 xc0=xc;yc0=yc;zc0=zc;
         for (int i = 0; i < nc; ++i) {
            track->GetElectron(i, xe, ye, ze, te, ee, dxe, dye, dze);
            // cout << "x,y,z,t ele = " << xe << ":" << ye << ":" << ze << ":" << te << endl;
            xtr.push_back(xe);
            ytr.push_back(ye);
            ztr.push_back(ze);
            ttr.push_back(te);

	    // default - if no DRIFT is defined 
	    xp = xe; yp = ye; zp = -1.0; tp = te;

#ifdef DRIFT_SIMPLE
            mc.eTransport(xe, ye, ze, te, xp, yp, zp, tp); // (xp,yp,zp,tp) = position/time at mesh
#endif

bool diffusion_man = false;

#ifdef MANUAL_DIFFUSION
         diffusion_man=true;
#endif

#ifdef DRIFT_RKF
	    // Integrate the drift/avalanche of this electron.
            driftRKF->DriftElectron(xe, ye, ze, te);
            // GetEndPoint RKF
            status = 0;
            driftRKF->GetEndPoint(xp, yp, zp, tp, status);
            //cout << "electron " << i << " ze " << ze << " zp " << zp << endl;            
            //cout << "electron " << i << " ye " << ye << " yp " << yp << endl;
            //cout << "         " << i << " xe " << xe << " xp " << xp << endl;
	    
       
       if(diffusion_man){
       //////////////////////////////////////////
	    // ROUGHLY EXTIMATED DIFFUSION L,T
	    //////////////////////////////////////////
         std::mt19937 e2(rd());
         double mean=0;
         double D=7.3745e-4;//cm^2/us ------ diffusion coefficient roughly extimated as D=mu*kT/e
         double sigma=sqrt(2*D*(tp-te)/1000);//cm
         std::normal_distribution<> dist(mean, sigma);
         //cout << "x before diff." << xp << endl;
         xp=xp+dist(e2);
         //cout << "x after diff." << xp << endl;
         //cout << "y before diff." << yp << endl;
         yp=yp+dist(e2);
         //cout << "y after diff." << yp << endl;
         double aux=dist(e2);
         zp=zp+aux;
         tp=tp+aux;
       }

#endif
            
#ifdef DRIFT_MC
            driftMC->DriftElectron(xe, ye, ze, te);
            status = 0;
	    // GetEndPoint MC
            driftMC->GetElectronEndpoint(0, xe1, ye1, ze1, te1, xe2, ye2, ze2, te2, status);
            //cout << "electron " << i << " ze  " << ze  << " ze  " << ze  << endl;
            //cout << "         " << i << " ze1 " << ze1 << " ze1 " << ze1 << endl;
            //cout << "         " << i << " xe2 " << xe2 << " xe2 " << xe2 << endl;
            xp = xe2; yp = ye2; zp = ze2; tp = te2;
#endif

            // cout << "x,y,z,t ele @ mesh = " << xp << ":" << yp << ":" << zp << ":" << tp << endl;
            xtrp.push_back(xp);
            ytrp.push_back(yp);
            ztrp.push_back(zp);
            ttrp.push_back(tp); //! tp becomes the t0 for the signal.
	    double egain = mc.eGain();
	    //cout << "Gain = " << egain << endl;
            nCharges.push_back(egain);
	    xEle[nEle] = xe; yEle[nEle] = ye; zEle[nEle] = ze; tEle[nEle] = te; 
	    xpEle[nEle] = xp; ypEle[nEle] = yp; zpEle[nEle] = zp; tpEle[nEle] = tp;
	    gEle[nEle] = egain;
	    if (nEle<nMaxEle) nEle++;
	    else cout << " nEle => nMaxEle" << endl;
         }
         esum += ec;
         netot += nc;
         //cout << "nc = " << nc << ": ec = " << ec << ": esum =" << esum << endl;
      }
      cout << "total number of electrons = " << netot << " " << xtrp.size() << ": tot E loss=" << esum << " track length = " << tlen << endl;

      // exec time stop -> show
      stop = high_resolution_clock::now(); 
      duration = duration_cast<microseconds>(stop - start);
      cout << "Exec time - loop over clusters = " << duration.count() << endl;
      /////////////////////////////////////////////
 
// skip signal generation if SIGNAL_OFF is defined 
#ifndef SIGNAL_OFF 

      //! In the previous version of the code (the same of the visualizer mctpc) we loop on every
      //! particle generated in the avalanche (we are assuming that all the
      //! particles are instantly splat on the resistive plate) and we track its spread on the
      //! resistive plate.
      //!
      //! The idea now is to generate the induced signal on the PADs avalanche by avalanche
      
      //vector<double>::iterator maxPtr;
      //vector<double>::iterator minPtr;
 
      //maxPtr = max_element( ttrp.begin(), ttrp.end() );
      //minPtr = min_element( ttrp.begin(), ttrp.end() );
 
      double tPadMax = *(max_element( ttrp.begin(), ttrp.end() ));
      double tPadMin = *(min_element( ttrp.begin(), ttrp.end() )); 
      cout << "Min tPad: " << tPadMin << "; Max tPad: " << tPadMax << ";" << endl;
      /*
      //double timeSpan = tPadMax - tPadMin + MCSamples*MCClockT;
 
      //int nBins = 1;
      //while (MCClockT*nBins < timeSpan) nBins++;

      // definisi solo per pad sopra soglia !!! // poi forse no nhistogrammi ma vettori ?
      for ( int i = 0; i < nrow*ncol; i++ ) {
         sprintf( histName, "pad%d", i );
         histVec.push_back( new TH1D( histName, histName, nBins, tPadMin, tPadMin + nBins*MCClockT ) );
      // histArr[i] = new TH1D( histName, histName, nBins, 0.0, nBins*MCClockT );
      }
      */

      // exec time start
      start = high_resolution_clock::now();
      /////////////////////////////////////////////
 
      //double sigmaAvl = mc.getSigmaAvalanche();
      double sigmaAvl = sqrt(subpheight * subpwidth / 12); 
      //double h = 1.0/(mc.getR()*mc.getC());
      cout << "SigmaAval = " << sigmaAvl << " cm, RC = " << RC << " [ns/cm2]" << endl;
      cout << "tts = sigmaAval**2 * RC = " << sigmaAvl*sigmaAvl*RC << " ... " << endl;
      double sigmaXYs2 = 0.0;
      double x0 = 0.0;
      double y0 = 0.0;
      double t0 = 0.0;
      double q0 = 0.0;
      //int flatIndex = 0;
      //int fICounter = 0;
      //vector<int> fIVec;
      int nsubpad = 0;
      subpadn.clear();
      subpadt.clear();
      subpadq.clear();
      subpadm.clear();
      bool loop;
      double deltat, subt, subm;
      int ipadwfs;
      int ipads;
      double xx0,yy0,tt0,qq0,tts;
      int index, index1, col0, row0, t0digit;
      int npad;
 
      for ( int i = 0; i < netot; ++i) { 
         x0 = xtrp.at(i);
         y0 = ytrp.at(i);
         if (x0<0 || y0<0 || x0>mmwidth || y0>mmheight) continue;        
         t0 = ttrp.at(i);
	 q0 = nCharges.at(i);
	 // cout << "charge amplif = " << q0 << endl;
         nsubpad = floor (x0/subpwidth) + nsubcols*floor (y0/subpheight);

	 //if(nsubpad<0) { cout << "\033[1;36m" << "nsubpad " << nsubpad << " negativo !!!" << x0 << " " << y0 << "\033[0m" << endl; }

         // double y00 = (floor (nsubpad / nsubcols) + 0.5) * subpheight;
         // double x00 = ((nsubpad % nsubcols) + 0.5) * subpwidth;
         // cout << "hit coord = " << x0 << " , "<< y0 << "sub pad coodr = " << x00 << " , " << y00 << endl; 

         pitr = subpadn.begin();
         loop = 1;
         while (loop) { // clusterizza elettroni arrivati sulle sub-pads in intervallo di tempo deltatmax
            itr=std::find(pitr, subpadn.end(), nsubpad);
            if (itr == subpadn.end()) {
               subpadn.push_back(nsubpad);
               subpadt.push_back(t0);
               subpadm.push_back(1);
               subpadq.push_back(q0);
               //std::cout << "Element not found -> add new one " << nsubpad << "t0 " << t0 << " tot elements = " << subpadn.size() << endl;
               loop = 0; // esci dal while loop
            } else {
               index = std::distance(subpadn.begin(),itr);
               subt = subpadt.at(index);
               deltat = t0-subt;
               if (abs(deltat) < deltatmax ) {
               //std::cout << "Element found subpad " << nsubpad << "(" << index << ")"<< " with t0 - tprev = " << deltat << endl;
                  subpadm.at(index) +=1;
		  subm = (float) subpadm.at(index);
		  //std::cout << "tprev = " << subpadt.at(index); 
                  subpadt.at(index) = (subt * (subm-1.0) + t0) / subm;
		  //std::cout << " -> tnew = " << subpadt.at(index) << endl; 
                  subpadq.at(index) += q0;
                  loop = 0; // trovato cluster cui l'elettrone i-esimo appartiene -> esci al while loop 
               } else { // continua nel loop alla ricerca di carica nel sub-pad con tempo compatibile con elettrone i-esimo
                  //std::cout << "Element found subpad " << nsubpad << "(" << index << ")"<< " with t0 - tprev = " << deltat << ">" << deltatmax << endl;
                  pitr = itr+1;
               }
            }
         }
      }
      cout << "n tot electrons = " << netot << " -> n tot subpads = " << subpadn.size() << endl;
      tPadMax = *(max_element( subpadt.begin(), subpadt.end() ));
      tPadMin = *(min_element( subpadt.begin(), subpadt.end() )); 
      cout << "Min t sub Pad: " << tPadMin << "; Max t sub Pad: " << tPadMax << ";" << endl;

      // exec time stop -> show
      stop = high_resolution_clock::now(); 
      duration = duration_cast<microseconds>(stop - start);
      cout << "Exec time - charge on sub pads = " << duration.count() << endl;
 
      // exec time start
      start = high_resolution_clock::now();
      /////////////////////////////////////////////
 
      t0d.clear();
      padn.clear();    // vettore con le pads attive (primarie e secondarie) di cui la forma d'onda in padwf
      padp.clear();    // vettore con le primary pads attive di cui la forma d'onda in padwf
      padwf.clear();   // vettore Q(t) con tutte le forme d'onda dell'evento (vettore di vettore)
      padwfi.clear();  // vettore I(t) con tutte le forme d'onda dell'evento (vettore di vettore)
      padwfdf.clear(); // vettore filter(I(i)) con tutte le forme d'onda dell'evento (vettore di vettore)
      padwfcv.clear(); // I(s) * T(s)
      ipadwfs  = 0;
      ipads    = 0;
      nSub = 0;
      index  = 0;
      index1 = 0;
      npad = 0;
 
      for (auto it = begin (subpadn); it != end (subpadn); ++it) { // loop sulle subpad attivate
         index = std::distance(subpadn.begin(),it); 
         yy0 = floor (*it / nsubcols) * subpheight;
         xx0 = (*it % nsubcols) * subpwidth;
         col0 = floor (xx0/pwidth); // sub pad -> in quale pad 
         row0 = floor (yy0/pheight);
         yy0 += 0.5*subpheight; // xx0,yy0 centrati in subpad
         xx0 += 0.5*subpwidth;
         tt0 = subpadt.at(index);
         qq0 = subpadq.at(index);
         t0digit = floor(tt0 / MCClockT);
	 t0d.push_back(t0digit);
         //cout << "sub pad coord = " << xx0 << " , " << yy0 << " time = " << tt0 << " q = " << qq0 << endl; 

	 xSub[nSub] = xx0;
	 ySub[nSub] = yy0;
	 tSub[nSub] = tt0;
	 qSub[nSub] = qq0;
	 //if (nSub < nMaxSub) nSub++;
	 nSub++;
 
         npad = col0 + ncol * row0;
	 //if(npad<0) {
         //   cout << "\033[1;31m" << "npad " << npad << " negativo !!!" << xx0 << " " << yy0 << "\033[0m" << endl;
         //   cout << "subpad # " << *it << "index " << index << endl;
	 //}
         if ((itr=std::find(padp.begin(), padp.end(), npad)) == padp.end()) padp.push_back(npad); 
         float xMin,xMax,yMin,yMax,kt;
         for (int icol=col0+dcol0;icol<=col0+dcol1;++icol) // loop sulle pads attorno alla pad principale
         if (icol > -1 && icol < ncol)            
         for (int irow=row0+drow0;irow<=row0+drow1;++irow) 
         if (irow > -1 && irow < nrow) { // loop su tutte le pads attorn a quella di interesse
            ++ipadwfs;
            npad = icol + ncol * irow;
            x1ipad = (double)(icol+1) * pwidth; 
            x0ipad = (double)  (icol) * pwidth; 
            y1ipad = (double)(irow+1) * pheight; 
            y0ipad = (double)  (irow) * pheight; 
            // calcola wf
            memset(&wf[0], 0, wf.size() * sizeof wf[0]); //azzera vettore wf ... faster than wf.assign(wf.size(),0);
	    //cout << "New wf " << endl;
            for ( int ts = t0digit; ts < MCSamples; ++ts ) { // absolute ts timeStamps with clock period MCClockT 
	       tts = (ts-t0digit) * MCClockT;
	       //cout << "tts = "  << tts << endl;
	       xMax=x1ipad-xx0; // x0+lx/2  
	       xMin=x0ipad-xx0; // x0-lx/2
	       yMax=y1ipad-yy0; // 
	       yMin=y0ipad-yy0; //
	       kt=tts * kk; // k*t = area
	       // 20210702 - nota bene segue la descrizione gia` della corrente -> non serve derivare successivamente
	       if (kt == 0) continue;
	       wf.at(ts)= qq0 * (kk/ (8.*sqrt(M_PI)*pow(kt,1.5))) *
		   ( (xMin*exp(-(xMin*xMin/(4*kt)))-xMax*exp(-(xMax*xMax)/(4*kt)))*(erf(-yMin/sqrt(4*kt))+erf(yMax/sqrt(4*kt))) +
                     (yMin*exp(-(yMin*yMin/(4*kt)))-yMax*exp(-(yMax*yMax)/(4*kt)))*(erf(-xMin/sqrt(4*kt))+erf(xMax/sqrt(4*kt))) );
            }
            // push_back wf
            if ((itr=std::find(padn.begin(), padn.end(), npad)) == padn.end()) { // questa pad non e` stata considerata in precedenza
               padn.push_back(npad);
               padwf.push_back(wf);
               ipads++;
             //std::cout << "Element not found -> add new one " << npad << " tot elemets = " << padn.size() << endl;
            } else { // questa pad e` gia` stata attivata (NB: in questo caso ogni pad ci deve essere solo una volta !!!)
               index1 = std::distance(padn.begin(),itr);
               for ( int ts = t0digit; ts < MCSamples; ++ts ) padwf[index1].at(ts) += wf.at(ts);
             //std::cout << "Element found subpad " << nsubpad << "(" << index << ")"<< "t0 - tprev = " << deltat << endl;
            }
         }
      }  
      cout << "sub pads for WF generation (x 2drow0+1 x 2dcol0+1) = " << ipadwfs << " new ipads " << ipads << endl;
      tPadMax = *(max_element( t0d.begin(), t0d.end() ));
      tPadMin = *(min_element( t0d.begin(), t0d.end() )); 
      cout << "Min t_digitized sub Pad: " << tPadMin << "; Max t sub Pad: " << tPadMax << ";" << endl;

      // from charge to current 
      for (auto ipadn = begin (padn); ipadn != end (padn); ++ipadn) { // loop sulle pad attivate
         index1 = std::distance(padn.begin(),ipadn);
         memset(&wf[0], 0, wf.size() * sizeof wf[0]); 
        // simple derivative 
	// 20210702 -> skip derivative
	 for (long unsigned int i = 1; i < padwf[index1].size() ;++i) {
	    //wf[i] = (padwf[index1][i]-padwf[index1][i-1]) / MCClockT; // derivative
	    wf[i] = padwf[index1][i]; // si tratta gia` di una corrente
	    //wf[i] = padwf[index1][i]; // debug
	 }
	 padwfi.push_back(wf);
      }

      padwfMax.clear();
      padwfMin.clear();
      double MaxpadwfMax;
      double MinpadwfMax;
      for (auto ip = begin (padwfi); ip != end (padwfi); ++ip) { // loop sulle pad wf 
         padwfMax.push_back (*(max_element( begin(*ip), end(*ip) )));
         padwfMin.push_back (*(min_element( begin(*ip), end(*ip) ))); 
      }
      MaxpadwfMax = *(max_element( padwfMax.begin(), padwfMax.end() ));
      MinpadwfMax = *(min_element( padwfMax.begin(), padwfMax.end() ));
      cout << "Min Ampl among I wf Maxima: " << MinpadwfMax << "; Max Ampl among I wf Maxima: " << MaxpadwfMax  << ";" << endl;

      // exec time stop -> show
      stop = high_resolution_clock::now(); 
      duration = duration_cast<microseconds>(stop - start);
      cout << "raw wf -> Exec time - waveform generation =" << duration.count() << endl;
      /////////////////////////////////////////////
        
      // exec time start
      start = high_resolution_clock::now();
      /////////////////////////////////////////////
 
      // digital semigaussian filter
      for (auto ipadn = begin (padn); ipadn != end (padn); ++ipadn) { // loop sulle pad attivate
         index1 = std::distance(padn.begin(),ipadn);
         memset(&wf[0], 0, wf.size() * sizeof wf[0]); 
         memset(&wf1[0], 0, wf1.size() * sizeof wf1[0]); 
        // high pass
	 for (long unsigned int i = 1; i < padwfi[index1].size() ;++i) {
	    wf[i] = dfb1*wf[i-1] + 
		    dfa0*padwfi[index1][i] + 
		    dfa1*padwfi[index1][i-1] +
		    padwfi[index1][i-1]/dfpz;
	 }
        // low pass
	 for (int np = 0; np < dfnp; np++) {
	    for (long unsigned int i = 1; i < padwfi[index1].size() ;++i) {
	       wf1[i] = dfb1*wf1[i-1] + dfa2 * wf[i];
            }
	    for (long unsigned int i = 1; i < padwfi[index1].size() ;++i) {
	      if(np == dfnp-1)  wf[i] = wf1[i] * dfgain; // last iteration -> x gain 
	      else wf[i] = wf1[i];
            }
         }
	 padwfdf.push_back(wf);
      }

      padwfMax.clear();
      padwfMin.clear();
      for (auto ip = begin (padwfdf); ip != end (padwfdf); ++ip) { // loop sulle pad wf 
         padwfMax.push_back (*(max_element( begin(*ip), end(*ip) )));
         padwfMin.push_back (*(min_element( begin(*ip), end(*ip) ))); 
      }
      MaxpadwfMax = *(max_element( padwfMax.begin(), padwfMax.end() ));
      MinpadwfMax = *(min_element( padwfMax.begin(), padwfMax.end() ));
      cout << "Filtered wf -> Min Ampl among wf Maxima: " << MinpadwfMax << "; Max Ampl among wf Maxima: " << MaxpadwfMax  << ";" << endl;

      cout << "... Temporary fix -> force filtered waveform A_Max to 4096 ... " << endl;
      AGain = 4096.0 / MaxpadwfMax; 

      // exec time stop -> show
      stop = high_resolution_clock::now(); 
      duration = duration_cast<microseconds>(stop - start);
      cout << "Exec time - fee response + add noise + digit =" << duration.count() << endl;
      /////////////////////////////////////////////
        
        
      // exec time start
      start = high_resolution_clock::now();
      /////////////////////////////////////////////
 
      // convolution
      for (auto ipadn = begin (padn); ipadn != end (padn); ++ipadn) { // loop sulle pad attivate
         index1 = std::distance(padn.begin(),ipadn);
         memset(&wf[0], 0, wf.size() * sizeof wf[0]); 
         memset(&wf1[0], 0, wf1.size() * sizeof wf1[0]); 
        // high pass
	 for (long unsigned int i = 0; i < padwfi[index1].size() ;++i) {
	    for (long unsigned int j = 0; j < i ;++j) {
	         wf[i] += padwfi[index1][i-j]*
	               3.0*( exp(-1.0*j/taus) + exp(-0.75*j/taus)*(sin((sqrt(7.0)*j)/(4.0*taus))/sqrt(7.0)-cos((sqrt(7.0)*j)/(4.0*taus))) );
	    }
	 }
	 padwfcv.push_back(wf);
      }

      padwfMax.clear();
      padwfMin.clear();
      for (auto ip = begin (padwfcv); ip != end (padwfcv); ++ip) { // loop sulle pad wf 
         padwfMax.push_back (*(max_element( begin(*ip), end(*ip) )));
         padwfMin.push_back (*(min_element( begin(*ip), end(*ip) ))); 
      }
      MaxpadwfMax = *(max_element( padwfMax.begin(), padwfMax.end() ));
      MinpadwfMax = *(min_element( padwfMax.begin(), padwfMax.end() ));
      cout << "Convoluted wf -> Min Ampl among wf Maxima: " << MinpadwfMax << "; Max Ampl among wf Maxima: " << MaxpadwfMax  << ";" << endl;

      cout << "... Temporary fix -> force filtered waveform A_Max to 4096 ... " << endl;
      AGain = 4096.0 / MaxpadwfMax; 

      // exec time stop -> show
      stop = high_resolution_clock::now(); 
      duration = duration_cast<microseconds>(stop - start);
      cout << "Exec time - fee response + add noise + digit =" << duration.count() << endl;
      /////////////////////////////////////////////
        
      ////////////////////////////////////////////////// WRITE DATA
      // exec time start
      start = high_resolution_clock::now();
      /////////////////////////////////////////////
 
      // Init Event -> fake event for test
      //TestEventWritePattern();
      timestamp = 0x111122220000 + nEvt;

      printf("size of datum = %ld size of buf[0] = %ld \n",sizeof(datum),sizeof(buf[0]));
 
      // Buffer reset (pointe back to start)   
      wbuf = 0;
   
      // Event header 
      datum = 0xFFFF & (unsigned short) PFX_START_OF_EVENT;  
      memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
   
      datum = 0xFFFF & ((timestamp & 0x00000000FFFF) >> 0);  
      memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
      datum = 0xFFFF & ((timestamp & 0x0000FFFF0000) >> 16);  
      memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
      datum = 0xFFFF & ((timestamp & 0xFFFF00000000) >> 32);  
      memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
   
      datum = 0xFFFF & ((nEvt & 0x0000FFFF) >> 0);  
      memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
      datum = 0xFFFF & ((nEvt & 0xFFFF0000) >> 16);  
      memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
   
      // Data 
      nPad = 0;
      memset(&adc[0], 0, adc.size() * sizeof adc[0]); //azzera vettore adc
      for (auto ipadn = begin (padn); ipadn != end (padn); ++ipadn) { // loop sulle pad attivate
         index1 = std::distance(padn.begin(),ipadn);
	 //adc.assign(padwf[index1].begin(),padwfdf[index1].end());
	 //cout << "ClockRatio = " << ClockRatio << " MCClockT, ADCClockT = " << MCClockT << "," << ADCClockT << endl;
	 //
	 long unsigned int j = 0;
	 // digitize digital filtered wf
	 //for (long unsigned int i = 0; i < padwfdf[index1].size() && j < adc.size() ;i+=ClockRatio) {
	 //   adc[j++] = padwfdf[index1][i] * AGain;
	 //}

	 // digitize convoluted wf
	 for (long unsigned int i = 0; i < padwfcv[index1].size() && j < adc.size() ;i+=ClockRatio) {
	    adc[j++] = padwfcv[index1][i] * AGain;
	 }

         yPad[nPad] = floor (*ipadn / ncol);
         xPad[nPad] = (*ipadn % ncol);
         dit = max_element(adc.begin(), adc.end());
         tPad[nPad] = std::distance(adc.begin(),dit);
         qPad[nPad] = *dit / AGain;
         //cout << "xPad " << xPad[nPad] << " yPad " << yPad[npad];
         //cout << "Max Adc " << qPad[nPad] << "; Max t Pad: " << tPad[nPad] << endl;
	 nPad++;

         int OT = 0;  // overthreshold status
         int OTT = 0; // transition over threshold
         int nPul = 0; // pulses over threshold
         int nDat = 0; // data count (should be even)
         unsigned short thisADC = 0;
         unsigned short prevADC = 0;
         for (long unsigned int nBin=0;nBin < adc.size(); ++nBin) {
            thisADC = adc[nBin];
            if (prevADC == 0 && thisADC > 0){
               OT = 1;  // over threshold
               OTT = 1; // over threshold transition
               nPul++;
               if (nPul == 1) { // write Card/Chip/Chap (once per chan if any data >0)
                  int x = *ipadn % ncol;
                  int y = floor (*ipadn / ncol);
                  int nCard = Card[x][y];
                  int nChip = Chip[x][y];
                  int nChan = Chan[x][y];
                  datum = 0xFFFF & (PFX_EXTD_CARD_CHIP_CHAN_HIT_IX);  
                  memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
                  nDat+=1;
                  //datum = 0xFFFF & (((0x1F & nCard) << 11) | ((0xF & nChip) << 7)  | (0xBF & nChan));  
                  datum = 0xFFFF & (((0x1F & nCard) << 11) | ((0xF & nChip) << 7)  | (0x7F & nChan));  
                  memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
                  nDat+=1;
               }
               if (OTT == 1) { // write Time Bin Index 
                  datum = 0xFFFF & (PFX_TIME_BIN_IX | (0x1FF & nBin));  
                  memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
                  nDat+=1;
                  OTT = 0;
               }
               // write ADC datum
               datum = 0xFFFE & (PFX_ADC_SAMPLE | (0xFFF & thisADC));  
               memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
               nDat+=0;
               prevADC = adc[nBin];
            } else if (prevADC > 0 && thisADC > 0){
               OT = 1;
               OTT = 0;
               // write ADC datum
               datum = 0xFFFE & (PFX_ADC_SAMPLE | (0xFFF & thisADC));  
               memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
               nDat+=0;
               prevADC = adc[nBin];
            } else if (prevADC > 0 && thisADC == 0){
               OT = 0;
               OTT = 0;
               prevADC = adc[nBin];
            } else {
               OT = 0;
               OTT = 0;
               prevADC = adc[nBin];
            }
         }
         if (nDat % 2 == 1) {
            datum = (unsigned short) 0x0;
            memcpy(buf+wbuf, &datum, sizeof(datum)); wbuf+=sizeof(datum);
         }
      }
    
      //printf("datum %x \n",datum);
      printf("buf len %ld \n",wbuf);
   
      //if (fwrite(buf,sizeof(uint8_t),wbuf,fpout) != 1)
        if (fwrite(buf,wbuf,1,fpout) != 1){ 
           printf("Error writing to file \n");
        }
   ////////////////////////////////////////////////// END WRITE DATA

      // exec time stop -> show
      stop = high_resolution_clock::now(); 
      duration = duration_cast<microseconds>(stop - start);
      cout << "Exec time - write aqs file =" << duration.count() << endl;
      /////////////////////////////////////////////
 
#endif // GARFIELD_ONLY

      // debug TTree fill
      t3->Fill();
 
   } // end loop on events  
#endif // if !defined(VIEW)
     
   // term IO
   fclose(fpout);
   TermIO();

   // debug TTree & Root close
#if DEBUG > 0
   t3->Print();
#endif
   froot.cd();
   t3->Write();
   froot.Close();

   // in case of dynamic allocation
   free(buf);

#ifdef TAPPLICATION
   //c1->SaveAs("plot.pdf");
   
   // need to create a ROOT application to draw a canvas, it does not need to be run
   //app.Run(true);

   std::cout << "Press return to continue ..." << std::endl;
   int flag = fcntl(1, F_GETFL, 0);
   int key = 0;
   while(true) {
     gSystem->ProcessEvents();
     fcntl(1, F_SETFL, flag | O_NONBLOCK);
     key = getchar();
     if((key == '\n') || (key == '\r')) break;
     usleep(1000);
   }
   fcntl(1, F_SETFL, flag);
#endif

   return 0;
}


