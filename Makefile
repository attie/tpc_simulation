SRCDIR	  = ./src
OBJDIR	  = ./obj
BINDIR	  = ./bin
INCDIR	  = ./inc
EXE 	  = tpcmc
GAS 	  = tpcgas
HEADERS   = $(wildcard $(INCDIR)/*.h)
SRCFILES  = $(wildcard $(SRCDIR)/*.c)
SRCFILES := $(filter-out $(SRCDIR)/$(EXE).c, $(SRCFILES))
SRCFILES := $(filter-out $(SRCDIR)/$(GAS).c, $(SRCFILES))
OBJFILES  = $(subst $(SRCDIR),$(OBJDIR),$(SRCFILES:.c=.o))

GFHOME = $(GARFIELD_HOME)
GFOBJDIR = $(GFHOME)/Object
GFSRCDIR = $(GFHOME)/Source
GFINCDIR = $(GFHOME)/install/include/Garfield
GFHEEDDIR = $(GFHOME)/install/Heed
GFLIBDIR  = $(GFHOME)/install/lib

#CERNLIB = `cernlib packlib mathlib`
CERNINC = $(ROOTSYS)/include 
          #/opt/applications/RootV2/root/include    
          #/usr/include 
          #/opt/applications/root_src/installdir/include
                       
# C++ compiler mc_tpc
#CXX = `root-config --cxx`
# C++ compiler TPCAnalysis
#CXX        	= g++ -std=c++11 -O2 -lm -lrt -lpthread -fPIE
CXX = g++ -std=c++11 -g -lm -lrt -lpthread -fPIE
# CXXflAGS TPCAnalysis
#CXXFLAGS   	= -W -Wall -Wextra -pedantic $(PYCCFLAGS) $(ROOTCFLAGS)
#CXXFLAGS   	= -W -Wall -Wextra -pedantic 
# CXXflAGS mc_tpc
CXXFLAGS = `root-config --cflags` \
	-g -W -Wall -Wextra -Wno-long-long \
	-fno-common \
	-I$(GFINCDIR) -I$(GFHEEDDIR) -I$(INCDIR) -I$(CERNINC)
#	-O3 -W -Wall -Wextra -Wno-long-long \
	
#LDFLAGS    	= $(PYLDFLAGS) $(ROOTLDFLAGS) $(ROOTLIBS) $(ROOTGLIBS) -lMinuit
LDFLAGS =`root-config --glibs` -lGeom -lgfortran -lm
LDFLAGS += -L$(GFLIBDIR) -lGarfield -lmagboltz
#LDFLAGS += `cernlib packlib mathlib`
LDFLAGS += `root-config --glibs` -lGeom -lgfortran -lm
LDFLAGS += `root-config --libs`
LDFLAGS += `root-config --cflags`

#MAINOBJS   =  ./src/MC_ResMM.c

default: build $(EXE) $(GAS)
	@echo "In default rule."

#tpcmc: tpcmc.c
#	$(CXX) $(CXXFLAGS) -o bin/tpcmc tpcmc.o $(MAINOBJS) $(LDFLAGS)
#	$(CXX) $(CXXFLAGS) -c src/tpcmc.c
	
# Build the main executables
#$(EXE): $(OBJFILES) 
$(EXE): $(OBJDIR)/$(EXE).o $(OBJFILES)
	@echo ""
	@echo "Linking $@ from $^"
	$(CXX) $(OBJFILES) $(OBJDIR)/$@.o -o $(BINDIR)/$@ $(LDFLAGS)
	#$(CXX) $(OBJFILES) $< -o $(BINDIR)/$@ $(LDFLAGS)

# Build the main executables
#$(GAS): $(OBJFILES)
$(GAS): $(OBJDIR)/$(GAS).o $(OBJFILES)
	@echo ""
	@echo "Linking $@ from $^"
	$(CXX) $(OBJFILES) $(OBJDIR)/$@.o -o $(BINDIR)/$@ $(LDFLAGS)
	#$(CXX) $(OBJFILES) $< -o $(BINDIR)/$@ $(LDFLAGS)

#$(EXE).o: $(EXE).cxx $(OBJFILES)
#$(OBJDIR)/$(EXE).o: $(SRCDIR)/$(EXE).c $(OBJFILES)
	#@echo ""
	#@echo "Compiling $< to $@"
	#@$(CXX) $(CXXFLAGS) -c $< -o $@

# Build the objects from the source files.
#$(OBJDIR)/%.o: $(SRCDIR)/%.c
$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@echo ""
	@echo "Compiling $< to $@"
	@$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: build
build:
	@mkdir -p $(OBJDIR)
	@mkdir -p $(BINDIR)

.PHONY: clean
clean:
	@echo "Cleaning..."
	rm -f $(OBJDIR)/*.o
	rm -f $(BINDIR)/$(EXE)
