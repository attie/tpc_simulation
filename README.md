## 1 - Sviluppo iniziale nelle due directories
* simulazione -> `/mnt/c/Users/colla/cernbox/DATA/T2K/MCTPC/mc_tpc-master`
* scrittura dati -> `~/wa/TPCWriteData/pandax_191011/projects/pandax/evwrite`
* debug -> t3 root + reader

## 2- Istruzioni

### Install garfield++
If you did not do it already, install `garfield++` on your machine.

The quickest way to do it (also by mimizing the compilation issues) is to avoid the compilation
of the examples:
```
~$ cd /where/you/want/to/install/it
~$ git clone https://gitlab.cern.ch/garfield/garfieldpp.git
~$ cd garfieldpp
~$ export GARFIELD_HOME=$( pwd )
~$ mkdir build; cd build
~$ cmake -DWITH_EXAMPLES=OFF $GARFIELD_HOME
~$ make -j[N of cores you want to use]
~$ make install
```

After this, __export the GARFIELD_HOME environment variable in your .bashrc__.

### Setup the correct environment

By sourcing the `setup.sh` script you will set the correct environment in order to successfully run the TPC simulation code.
```
~$ source setup.sh
```

The environment variable `TPCMC` is created for your convenience and the `LD_LIBRARY_PATH` updated.

__REMEMBER:__ put the environment variable `GARFIELD_HOME` into your .bashrc:
```
export GARFIELD_HOME=/path/to/your/garfieldpp
```

### Compile the code

Run the makefile

```
~$ make
```

__CAUTION:__ if you get a compilation error that says something about a member function called `EnableMagneticField` then it means you have an newer version of Garfield.
The field is now enable by default and this member function does not exist anymore. If this is your case, just replace
```c++
#undef GARFIELD_LATEST 
```
with
```c++
#define GARFIELD_LATEST
```
in the `tpcmc.c` file.
The reason why the `#undef` is the default state is to mantain retro-compatibility with previous versions of Garfield, once everyone working on this code
installs the current version of Garfield this can be removed altogether from the code.

### Setup the event 
If you want to change the track starting direction, energy, particle type etc... you have to modify directly the file `tpcmc.c`.
At line 376 you can set the direction: 
x->horizontal axis of anode plane (from to 42 cm)
y->vertical axis (from 0 to 36 cm)
z->drift axis (from -98 to 0 cm)

Particle type and energy->line 1108
Number of events->line 178
If you want to generate random directions per each track, you can 
```c++
#define RANDOM_TRACKS
```
and modify the particle directions from line 1112.

In order to enable/disable magnetic field you can modify parameters from line 427

__CAUTION:__ You have to compile again the files and then run the code.
You can do both steps together by moving to /bin directory and then:
```
source script.sh
```


### Run the code

From wherever you are on your machine, you can run the executable
```
~$ $TPCMC/bin/tpcmc
```
This will save the output ROOT file in the current directory from which you have run the program.


## 3 - ROOT
```
 f.Close();TFile f("tpcmc.root", "READ")
 t3->Draw("yp:xp","id==0")
 t3->Draw("yp:zp:xp","id==0")
 t3->Draw("ys:ts:xs","id==0")
 t3->Draw("padwf[101]:Iteration$","id==1")
 t3->Draw("padwf:Iteration$","id==1")
 t3->Draw("floor(padn/36):padn%36","id==4","box")
 t3->Draw("ys:xs","id==2","box")
 t3->Draw("ys:xs","id==0")
 t3->Draw("ye:xe","id==0","same")
 t3->Draw("(ypd+0.5)*1.009:(xpd+0.5)*1.118","id==0","same")
 .x t3read.C
```

## NOTE
- Mapping dir -> "../src/Mapping" !!! hardcoded in T2KConstrants.h (../ va bene se exe sta in src/../bin)
- mapping dir -> messo indirizzo assoluto... 
- readout corretto con TPCWriteData (TPCAnalysis ha qualche problema con controllo su numer evento)

## NOTE Aug 2021
- impostato gas -> ExBxAngle -> altrimenti nonporpaga correttamente elettroni
- impostati campi -> diverse opzioni 
	- Semplice -> verisone originale -> solo campi costanti
	- User -> funzioni (lkambda)
	- Voxel -> OK... ma necessario molto debug
		1) meglio se la mesh corrispone al file... numero bin e limiti
		2) ma quando corrisponde perfettamente... ci sono problemi di arrotondamento -> alcuni bin non sono letti
		-> minimizzato il problema con piccole correzioni ai limiti dei ranges...
	- si possono impostare varie componenti per E e per B -> se non ci sono errori i campi vengono sommati
		... ci sono pero` finti errori dovuti al catto che se in una componente c'e' solo B, al momento di leggere E questo genera un errore interno alla letutra della componente
		... non e` un problema E viene letto da altra componente ... ho debuggato e mi sembra OK -> ho ritotto output per non avere problemi
		-> rimane errore in output quando si e` fuori Mesh per la componente Voxel... accade quando si disegna con range piu` ampio
	... per debug ho importato vari files Garfield .cc e relativi .hh in questo codice (sono diventati .c e .h)  
- impostate varie modalita` di propagazione elettroni -> integrazione (manca la diffusione) o montecarlo (include la diffusione) 
- Physics 
	-> impostati studi di deformazioni per non uniformita` E field
	-> impostata lettura campo B di DESY -> implementato il campo qui. Si puo` combinare con campo elettrico -> attenzione alla normalizzazione di B field !!! 


## TODO list

- DriftRKF -> aggiungere la diffusione
- carica da rinormalizzare -> ??? c'e' limite in write waveform ???
- tempi iniziali da capire -> lo spread e` molto piccolo ma si trovano lontane !!!
