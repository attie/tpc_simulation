#include "TGraphAsymmErrors.h"
#include "TString.h"
#include "TFile.h"
#include "TH1F.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TMath.h"
#include "SetT2KStyle.hxx"

//#include "Math/SpecFuncMath.h"

#include <iostream>
#include <fstream>
#include <cmath>

#include <RooRealVar.h>
#include <RooExponential.h>
#include <RooGaussian.h>
#include <RooFormulaVar.h>
#include <RooAddPdf.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooArgSet.h>
#include <RooArgList.h>
#include <RooPlot.h>
#include <RooErrorVar.h>
#include <RooMCStudy.h>
#include <RooFitResult.h>
#include <RooMinuit.h>

using namespace std;

void GetDeltaY(int z, int y){

  TString mag;
  double B=0.2;

  std::ostringstream stz;
  std::string zstr;
  stz << z;
  zstr = stz.str();

  std::ostringstream sty;
  std::string ystr;
  sty << y;
  ystr = sty.str();

  std::ostringstream stB;
  std::string Bstr;
  if(B<1){
    stB << B*10;
    Bstr = stB.str();
    mag= "0"+TString(Bstr);
  }
  else{
    stB << B;
    Bstr = stB.str();
    mag=TString(Bstr);
  }
  if(B==0){B=1e-20;}

  TString file_name;
  TString zero_file_name;
  TString input_prefix = "/media/lorenzo/Maxtor/TPC-GARFIELD/TPCMC_solidbox_at_negative_z/bin/tpcmc_";// "/home/lorenzo/T2K/Analysis/desy_testbeam/OutputFiles/";
  file_name = input_prefix + "z" + TString(zstr) + "_y" + TString(ystr) + "_B" + mag + ".root";

  TFile *inFile=NULL;
  inFile=TFile::Open(file_name);
  if (inFile->IsOpen()){
    cout << "Processing: " << file_name << " ... " << endl;
  }
  else{
    cout<<"failed opening of" << file_name <<endl; 
  }
  TTree *tree= (TTree*)(inFile->Get("t3"));

  int Ne;
  tree->SetBranchAddress("ne",&Ne);
  tree->GetEntry(1);
  double vec_y[Ne];
  double vec_x[Ne];


  tree->SetBranchAddress("yp",&vec_y);
  tree->SetBranchAddress("xp",&vec_x);
  tree->GetEntry(1);

  double yMax=0;
  double yMin=1000000;
  cout <<"size of y vector: "<<Ne <<endl;
  for (int i=0;i<Ne;i++){
    if(vec_x[i]<0.2){
      if(yMax<vec_y[i]){
        yMax=vec_y[i];
      }
    }
    if(vec_x[i]>41){
      if(yMin>vec_y[i]){
        yMin=vec_y[i];
      }
    }
    //cout <<i << ": "<<vec_y[i]<<"  -  ";
  }

  std::ofstream outFile;
  outFile.open("Deltas.txt",std::ofstream::app);

  outFile << z << "\t" << yMax-yMin <<endl;
  cout << "\n--------------------------------------\n" <<z << "\t" << yMax-yMin << " cm"<<endl;

  return;

}


void DG(){

  ////////////////////////////////////////////////STYLE//////////////////////////////////////
  Int_t T2KstyleIndex = 3;
  // Official T2K style as described in http://www.t2k.org/comm/pubboard/style/index_html
  TString localStyleName = "T2K";
  // -- WhichStyle --
  // 1 = presentation large fonts
  // 2 = presentation small fonts
  // 3 = publication/paper
  Int_t localWhichStyle = T2KstyleIndex;

  TStyle* t2kstyle = T2K().SetT2KStyle(localWhichStyle, localStyleName);
  //gROOT->SetStyle(t2kstyle->GetName());
  //gROOT->ForceStyle();
  ///////////////////////////////////////////////////////////////////////////////////////////



  string HASH ("#");

  const char* filename="/media/lorenzo/Maxtor/TPC-GARFIELD/TPCMC_solidbox_at_negative_z/bin/Deltas.txt";

  TCanvas *Graphcanvas = new TCanvas;
  Graphcanvas->cd();
  cout<<"Opening "<<filename<<"... ";
  ifstream input(filename);
  if(!input){
    cout<<"failed!"<<endl;
    return;
  }
  cout<<"succeed!"<<endl;

  vector<double> Z;
  vector<double> DeltayMax;
  
  
  while(input.good() && !input.eof() ){
    string rl;
    getline( input, rl);
    if( rl.empty() ) continue;
    if( rl.substr(0,1) == HASH )  continue;

    istringstream strm (rl);
    
    double a,b;

    strm>>a;
    strm>>b;

    Z.push_back(a);
    DeltayMax.push_back(b);
  }

  int n=Z.size();

  vector<double> Zreal(n);
  for (int i=0;i<n;i++){
    Zreal[i]=Z[i]+90.;
  }

  TGraph *g = new TGraph(n,&Zreal[0],&DeltayMax[0]);
  g->SetTitle((const char *)("MC"));//title
  g->GetXaxis()->SetTitle("distance from cathode");
  g->GetYaxis()->SetTitle("#Delta y");//#theta
  g->Draw("ap*");

  return;

}