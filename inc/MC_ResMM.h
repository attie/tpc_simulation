// For input numeric values be careful to the units employed
// by Garfield++.
//
 
#ifndef MC_RESMM_H
#define MC_RESMM_H

#include <stdio.h>

#include <TRandom3.h>
#include <TMath.h>
#include <TF1.h>
#include <TF2.h>
#include <TMatrixD.h>
#include <iostream>

 
class MC_ResMM{
 public:
   MC_ResMM();                  // function called the default constructor

   class MMExcep {
    public:
      MMExcep() {};
      ~MMExcep() {};
   }; 

   void TrackDir(double& x, double& y, double& z, double& dx, double& dy, double& dz);  // Set start point and direction of track
   void SetChTransport(double dv, double dl, double dt, double sigmaAvlWeight=1.0); // Set the transport parameters for electrons in the gas: dv=drift velocity [cm/us],
   // dl=long. diffusion coefficient  [cm/sqrt(cm)], dl=transv. diffusion coefficient  [cm/sqrt(cm)], 
   void SetRNDMeth(int meth = 0){RNDMeth = meth;}; // Methode to create track direction: meth=0 => fixed track values, meth=1 => randomize x,z and direction 
   void SetRNDRange(double xmin, double xmax, double y, double zmin, double zmax);  // set ranges and values for RND meth=1
   void eTransport( double x0, double y0, double z0, double t0, double& xpad, double& ypad, double& zpad, double& tpad);  // electron transport from creation point
   // x0,y0,z0: creation point; t0: time of creation, xyzt_pad: end point and time when electron reaches pad plane
   int eGain();  // randomize gain for each electron following a polya distribution
   bool eSpread(double x0, double y0, double t0, double t, double& xs, double& ys);  // WARNING: tries to spread the charge and does it but values to be used not clear/well chosen
   void angle2dir(double phi, double theta, double& dx, double& dy, double& dz); // simple function to convert angles into coordinate direction, just for convinience 
  
   void generateGrid( int npx, int npy, double lpx, double lpy ); // Generate grid geometry matrix
//  bool padSignals( double x0, double y0, double t0, double t, double* sigs[] );

   //! Getters
   //!
   const TMatrixD& getPadGridX() { return *padGridX; };
   const TMatrixD& getPadGridY() { return *padGridY; };
   double getR() { return R_MM; };
   double getC() { return C_MM; };
   double getSigmaAvalanche();


 private:

   bool setTransport;

   double MeanGain = 1000.;  // personal choice of gain 1000 based on MM experience from testbeam
   double PolyPara = 2.3;  // Polya parameter which I chose based on some slides about MM gain I found to 2.3
   double DLong;
   double DTrans;
   double sigmaAvalanche;
   double DriftZ;
   double DriftVelo;
   int RNDMeth = 0;
   double Xmin, Xmax, Ypos, Zmin, Zmax;
   double polyaInt[1000];
   double CpolyaInt[1000];
   int CalcPolyInt = 0;
   double RatioGain = 0.;
   int nbingain = 1000;
   //! C should be in units of Capacitance/unit area.
   //! R should be a simple resistance value ... (?)
   //!
   double R_MM = 2.5e6; // was 2500000.
   double C_MM = 4.0e-9; // was 0.000000004
   double tau = R_MM*C_MM;
   double h = 1.0/tau;

   //! MM geometry.
   //! Length unit is cm.
   //!
   double amplGap = 128e-04; //cm
   int nPadsX, nPadsY;
   double lPadX, lPadY;
   TMatrixD* padGridX;
   TMatrixD* padGridY;

   void CalcPolyaIntegral();

   TRandom3 *xrnd = new TRandom3;
   TRandom3 *zrnd = new TRandom3;
   TRandom3 *u = new TRandom3;
   TRandom3 *v = new TRandom3;
   TRandom3 *rrnd = new TRandom3;
   TRandom3 *phirnd = new TRandom3;

   double SpreadInt[3000];
   double CSpreadInt[3000];
   double RMAX = 20.;

   //! This parameter is used in the computation of the
   //! charge spread.
   int nbinspread = 3000;

   int last_t = 0;
   void CalcSpreadInt(double t0, double t);

   static bool debug; 

};


#endif
