#ifndef EVWRITER_H
#define EVWRITER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <stdint.h>   /* C99 compliant compilers: uint64_t */
#include <ctype.h>    /* toupper() */
#include <sys/time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <assert.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>        /* necessaria per tm- structure (dereferencing pointer to incomplete type)*/
#include <sched.h>
#include <pthread.h>
#include <semaphore.h>
#include <setjmp.h>
#include "zlib.h"
#include <unistd.h>   // getch_
#include <termios.h>  // getch_

#endif
