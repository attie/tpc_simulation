#!/bin/bash

[ $0 = ${BASH_SOURCE[0]} ] && {
   printf "ERROR: you have to source this script!\n"
} || {

   TPCMC=$( readlink -f ${BASH_SOURCE[0]} )
   TPCMC=${TPCMC%/*}
   
   export TPCMC
   
   [ -z "$GARFIELD_HOME" ] && {
      printf "You HAVE to define and export GARFIELD_HOME environment variable:\n"
      printf "Put \"export GARFIELD_HOME=/path/to/garfieldpp\" in your .bashrc\n"
      printf "*** Re-run setup.sh after having done that! ***\n"
   } || {
      [ $LD_LIBRARY_PATH = "${LD_LIBRARY_PATH#*$GARFIELD_HOME}" ] && {
         export LD_LIBRARY_PATH=$GARFIELD_HOME/install/lib:"$LD_LIBRARY_PATH"
      }
   }

   [ -z "$( grep '/source/setup.sh/please/' $TPCMC/inc/T2KConstants.h )" ] && {
      printf "File $TPCMC/inc/T2KConstants.h already setup correctly\n"
   } || {
      sed -e 's!/source/setup.sh/please/!"'$TPCMC'/"!' $TPCMC/inc/T2KConstants.h > inc/T2KConstants_new.h
   
      mv $TPCMC/inc/T2KConstants.h $TPCMC/inc/bku_T2KC.h
      mv $TPCMC/inc/T2KConstants_new.h $TPCMC/inc/T2KConstants.h
   }

}
