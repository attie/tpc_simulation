import matplotlib.pyplot as plt
import numpy as np
import io
import matplotlib.cm as cm
from matplotlib.colors import Normalize
import math

	
x = []
y = []
z = []
Bx = []
By = []
Bz = []
x, y, z, Bx, By, Bz = np.loadtxt('/media/lorenzo/Maxtor/TPC-GARFIELD/TPCMC_solidbox_at_negative_z/bin/bfield_files/bfield_+++_206.25_126.65_-492.00__7_7_10_-50_470_-50_470_-1050_50.txt',
                    unpack=True)
                    #/media/lorenzo/Maxtor/TPC-GARFIELD/TPCMC_solidbox_at_negative_z/bin/bfield_files/
                    #bfield__206.25_126.65_498.25__100_100_100_-50_470_-50_470_-50_1050.txt
                    #prova.txt

magnitude = []
magnitude_xy = []

n=len(x)
n3=n*n*n
colors = [(0, 0, 0.5)]

for i in range(n):
    magnitude.append( math.sqrt(Bx[i]*Bx[i]+By[i]*By[i]+Bz[i]*Bz[i]) )
    magnitude_xy.append( math.sqrt(Bx[i]*Bx[i]+By[i]*By[i]) )

    
ax = plt.figure().add_subplot(projection='3d')
for x1,y1,z1,Bx1,By1,Bz1,magnitude1 in zip(x,y,z,Bx,By,Bz,magnitude_xy):

		print("1")
		colors=[(  (magnitude1-np.amin(magnitude_xy))/(np.amax(magnitude_xy)-np.amin(magnitude_xy))  ,  1. - (magnitude1-np.amin(magnitude_xy))/(np.amax(magnitude_xy)-np.amin(magnitude_xy))  ,  0  )]
		#print(colors)
		ax.quiver(x1, y1, z1, Bx1, By1, Bz1/100,color=colors, length=magnitude1*1000)  #divide Bz1 by 100 and multiply length by 100 to show radial component


ax1 = plt.figure().add_subplot(projection='3d')
for x1,y1,z1,Bx1,By1,Bz1,magnitude1 in zip(x,y,z,Bx,By,Bz,magnitude_xy):

	if(z1>-50):
		print("2")
		colors=[(  (magnitude1-np.amin(magnitude_xy))/(np.amax(magnitude_xy)-np.amin(magnitude_xy))  ,  1. - (magnitude1-np.amin(magnitude_xy))/(np.amax(magnitude_xy)-np.amin(magnitude_xy))  ,  0  )]
		#print(colors)
		ax1.quiver(x1, y1, z1, Bx1, By1, Bz1/100,color=colors, length=magnitude1*7000)  #divide Bz1 by 100 and multiply length by 100 to show radial component


for i in range(n):
    magnitude.append( math.sqrt(Bx[i]*Bx[i]+By[i]*By[i]+Bz[i]*Bz[i]) )
    
    
ax2 = plt.figure().add_subplot(projection='3d')
for x1,y1,z1,Bx1,By1,Bz1,magnitude1 in zip(x,y,z,Bx,By,Bz,magnitude):

		print("3")
		colors=[(  (magnitude1-np.amin(magnitude))/(np.amax(magnitude)-np.amin(magnitude))  , 1. - (magnitude1-np.amin(magnitude))/(np.amax(magnitude)-np.amin(magnitude))   ,  0  )]#
		#print(colors)
		ax2.quiver(x1, y1, z1, Bx1, By1, Bz1,color=colors, length=magnitude1*15)  #divide Bz1 by 100 and multiply length by 100 to show radial component



plt.show()


